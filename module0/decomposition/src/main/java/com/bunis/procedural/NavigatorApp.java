package com.bunis.procedural;

import java.util.Scanner;

public class NavigatorApp {

    private static final int maxX = 10;
    private static final int maxY = 10;

    private static int currentX;
    private static int currentY;

    public static void main(String[] args) {

        boolean isRunning = true;
        while (isRunning) {
            isRunning = runLevel1();
        }
    }

    private static boolean runLevel1() {
        displayLevel1Commands();
        String nextCommand = readUserCommand();

        switch (nextCommand) {
            case "exit":
                return false;
            case "move":
                runLevel2();
                return true;
            case "reset":
                currentX = currentY = 0;
                System.out.println("Reset complete");
                return true;
            default:
                System.out.println("Are you kidding me?!");
                return true;
        }
    }

    private static String readUserCommand() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    private static void runLevel2() {

        boolean isRunning = true;
        while (isRunning) {
            displayLevel2Commands();
            final String command = readUserCommand();
            switch (command) {
                case "UP":
                    currentY += 1;
                    if (isPositionInvalid()) {
                        currentY -= 1;
                    }
                    break;
                case "DOWN":
                    currentY -= 1;
                    if (isPositionInvalid()) {
                        currentY += 1;
                    }
                    break;
                case "LEFT":
                    currentX -= 1;
                    if (isPositionInvalid()) {
                        currentX += 1;
                    }
                    break;
                case "RIGHT":
                    currentX += 1;
                    if (isPositionInvalid()) {
                        currentX -= 1;
                    }
                    break;
                case "exit":
                    isRunning = false;
                    break;
                default:
                    System.out.println("Are you kidding me?!");
                    break;
            }
        }
    }

    private static boolean isPositionInvalid() {
        return currentX > maxX || currentY > maxY;
    }

    private static void displayLevel2Commands() {
        System.out.println("You are here: x = " + currentX + " y = " + currentY);
        System.out.println("Choose your direction to move:");
        System.out.println("- UP");
        System.out.println("- DOWN");
        System.out.println("- LEFT");
        System.out.println("- RIGHT");
    }

    private static void displayLevel1Commands() {
        System.out.println("Hello in Navigator App");
        System.out.println("You may do following:");
        System.out.println("- move");
        System.out.println("- reset");
        System.out.println("- exit");
        System.out.println("Enter your command");
    }
}
