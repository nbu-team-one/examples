package com.bunis.oop.navigation;

import com.bunis.oop.entity.FlatPoint;

public class PointNavigator {

    private PositionRepairer repairer;
    private PositionResolver resolver;

    public PointNavigator(PositionRepairer repairer, PositionResolver resolver) {
        this.repairer = repairer;
        this.resolver = resolver;
    }

    public void move(FlatPoint point, PointDirection direction) {

        resolver.resolveNext(point, direction);
        repairer.repair(point);
    }
}
