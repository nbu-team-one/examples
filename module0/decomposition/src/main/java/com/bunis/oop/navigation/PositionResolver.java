package com.bunis.oop.navigation;

import com.bunis.oop.entity.FlatPoint;

public interface PositionResolver {

    void resolveNext(FlatPoint point, PointDirection direction);
}
