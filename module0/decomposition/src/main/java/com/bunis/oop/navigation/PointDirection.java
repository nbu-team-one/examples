package com.bunis.oop.navigation;

public enum PointDirection {

    UP, DOWN, LEFT, RIGHT
}
