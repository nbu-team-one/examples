package com.bunis.oop.entity;

public class VolumePoint extends FlatPoint {

    private int z;

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
