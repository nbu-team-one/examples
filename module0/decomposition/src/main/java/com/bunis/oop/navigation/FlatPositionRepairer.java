package com.bunis.oop.navigation;

import com.bunis.oop.entity.FlatPoint;

public class FlatPositionRepairer implements PositionRepairer {

    private int maxX;
    private int maxY;

    public FlatPositionRepairer(int maxX, int maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
    }

    @Override
    public void repair(FlatPoint point) {

        if (point.getX() > maxX) {
            point.setX(maxX);
        }
        if (point.getY() > maxY) {
            point.setY(maxY);
        }
        if (point.getY() < 0) {
            point.setY(0);
        }
        if (point.getX() < 0) {
            point.setX(0);
        }
    }
}
