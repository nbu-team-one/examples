package com.bunis.oop;

import com.bunis.oop.entity.FlatPoint;
import com.bunis.oop.navigation.FlatPositionRepairer;
import com.bunis.oop.navigation.FlatPositionResolver;
import com.bunis.oop.navigation.PointDirection;
import com.bunis.oop.navigation.PointNavigator;

import java.util.Scanner;

public class NavigatorApp {
    public static void main(String[] args) {

        new NavigatorApp().navigate();
    }

    private void navigate() {
        FlatPoint point = new FlatPoint();

        FlatPositionResolver resolver = new FlatPositionResolver();
        FlatPositionRepairer repairer = new FlatPositionRepairer(10, 10);
        PointNavigator navigator = new PointNavigator(repairer, resolver);

        boolean isRunning = true;
        while (isRunning) {

            System.out.println("You are here: x = " + point.getX() + " y = " + point.getY());
            System.out.println("Choose your direction");
            final String command = readUserCommand();
            switch (command) {
                case "UP":
                    navigator.move(point, PointDirection.UP);
                    break;
                case "DOWN":
                    navigator.move(point, PointDirection.DOWN);
                    break;
                case "LEFT":
                    navigator.move(point, PointDirection.LEFT);
                    break;
                case "RIGHT":
                    navigator.move(point, PointDirection.RIGHT);
                    break;
                case "exit":
                    isRunning = false;
                    break;
                default:
                    System.out.println("Try one more time");
                    break;

            }
        }
    }

    private String readUserCommand() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }
}
