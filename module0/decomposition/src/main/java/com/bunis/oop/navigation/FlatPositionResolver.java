package com.bunis.oop.navigation;

import com.bunis.oop.entity.FlatPoint;

public class FlatPositionResolver implements PositionResolver {
    @Override
    public void resolveNext(FlatPoint point, PointDirection direction) {

        switch (direction) {
            case UP:
                point.setY(point.getY() + 1);
                break;
            case DOWN:
                point.setY(point.getY() - 1);
                break;
            case LEFT:
                point.setX(point.getX() - 1);
                break;
            case RIGHT:
                point.setX(point.getX() + 1);
                break;
        }
    }
}
