package com.bunis.oop.navigation;

import com.bunis.oop.entity.FlatPoint;

public interface PositionRepairer {

    void repair(FlatPoint point);
}
