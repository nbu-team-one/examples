package com.bunis.tool;

public class Hammer extends HeavyTool {

    public Hammer() {
        super(10.0);
    }

    private void doHammerWork() {

    }

    @Override
    public void doToolWork() {
        this.doHammerWork();
    }
}
