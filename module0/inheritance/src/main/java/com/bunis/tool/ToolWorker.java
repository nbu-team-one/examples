package com.bunis.tool;

public class ToolWorker<H extends HeavyTool> implements Worker<H> {

    private H tool;

    public ToolWorker(H tool) {
        this.tool = tool;
    }

    @Override
    public void doWork() {
        tool.doToolWork();
    }

    @Override
    public H getTool() {
        return null;
    }

    @Override
    public void doAnotherWork() {

    }

    void changeTool(ToolWorker<? super H> worker) {


    }
}