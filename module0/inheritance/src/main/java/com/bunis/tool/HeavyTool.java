package com.bunis.tool;

public abstract class HeavyTool implements Tool {

    protected double weight;

    public HeavyTool(double weight) {
        this.weight = weight;
    }
}
