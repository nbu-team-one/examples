package com.bunis.tool;

public interface Worker<T extends Tool> {

    void doWork();

    T getTool();

    default void doAnotherWork() {
        final T tool = getTool();
        tool.doToolWork();

    }
}
