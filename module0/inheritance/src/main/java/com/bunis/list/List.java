package com.bunis.list;

public interface List<T> {

    boolean add(T t);

    boolean remove(T t);

    int size();
}
