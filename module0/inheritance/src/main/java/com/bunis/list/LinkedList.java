package com.bunis.list;

public class LinkedList<T> implements List<T> {

    private int size;
    private LinkedNode<T> tail;
    private LinkedNode<T> head;


    @Override
    public boolean add(T t) {

        if (t == null) {
            return false;
        }

        if (head == null) {
            head = new LinkedNode<>();
            head.value = t;
            tail = head;
        } else {
            LinkedNode<T> node = new LinkedNode<>();
            this.tail.next = node;
            node.previous = this.tail;
            node.value = t;
            this.tail = node;
        }

        this.size++;
        return true;
    }

    @Override
    public boolean remove(T t) {

        if (head == null) {
            return false;
        }

        if (head.value.equals(t)) {
            if (head.next != null) {
                head.next.previous = null;
            } else {
                tail = null;
            }

            head = head.next;
            size--;
            return true;
        } else if (tail.value.equals(t)) {
            tail.previous.next = null;
            tail = tail.previous;
            size--;
            return true;
        } else {
            LinkedNode<T> toRemove = head.next;
            while (toRemove != null) {
                if (toRemove.value.equals(t)) {
                    toRemove.previous.next = toRemove.next;
                    toRemove.next.previous = toRemove.previous;
                    size--;
                    return true;
                } else {
                    toRemove = toRemove.next;
                }
            }
        }

        return false;
    }

    @Override
    public int size() {
        return this.size;
    }

    private class LinkedNode<T> {

        private LinkedNode<T> previous;
        private LinkedNode<T> next;
        private T value;
    }
}
