package com.bunis.list;

public class Main {

    public static void main(String[] args) {

        List<Integer> list = new LinkedList<>();

        list.add(1);
        list.remove(1);
        System.out.println(list.size());

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        list.remove(1);
        System.out.println(list.size());
        list.remove(5);
        System.out.println(list.size());
        list.remove(3);
        System.out.println(list.size());
        list.remove(6);
        System.out.println(list.size());
    }
}
