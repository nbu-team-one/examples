package com.bunis.rc;

public class RemoteControl {

    private final ButtonBoard board;

    public RemoteControl(ButtonBoard board) {
        this.board = board;
    }

    void pressButton(String buttonName) {
        Button button = board.getButton(buttonName);
        button.press();
    }
}
