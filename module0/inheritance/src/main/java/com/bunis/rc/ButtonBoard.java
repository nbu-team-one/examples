package com.bunis.rc;

public interface ButtonBoard {

    Button getButton(String name);
}
