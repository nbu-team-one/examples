package com.bunis.rc;

public enum ButtonKey {

    BUTTON_1("1"),
    BUTTON_2("2"),
    BUTTON_3("3"),
    BUTTON_4("4"),
    BUTTON_ON("ON"),
    BUTTON_OFF("OFF");

    private final String key;

    ButtonKey(String key) {
        this.key = key;
    }

    public static ButtonKey fromKey(String name) {

        final ButtonKey[] values = ButtonKey.values();
        for (ButtonKey value : values) {
            if (value.key.equalsIgnoreCase(name)) {
                return value;
            }
        }
        return null;
    }
}
