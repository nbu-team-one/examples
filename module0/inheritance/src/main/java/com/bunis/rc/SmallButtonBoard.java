package com.bunis.rc;

import java.util.EnumMap;
import java.util.Map;

public class SmallButtonBoard implements ButtonBoard {

    Map<ButtonKey, Button> buttonMap = new EnumMap<>(ButtonKey.class);

    public SmallButtonBoard() {
        init();
    }

    private void init() {
        buttonMap.put(ButtonKey.BUTTON_OFF, new ButtonOff());
        buttonMap.put(ButtonKey.BUTTON_ON, new ButtonOn());
    }

    @Override
    public Button getButton(String name) {

        ButtonKey buttonKey = ButtonKey.fromKey(name);
        if (buttonMap.containsKey(buttonKey)) {
            return buttonMap.get(buttonKey);
        } else {
            final Button button = new Button() {
                @Override
                public void press() {
                    System.out.println("Я не знаю что это и где я! я хочу домой");
                }
            };
            return button;
        }
    }
}
