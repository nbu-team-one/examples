package com.bunis.rc;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        final SmallButtonBoard board = new SmallButtonBoard();
        RemoteControl control = new RemoteControl(board);

        boolean isRunning = true;
        while (isRunning) {
            Collection<String> list = new ArrayList<>();
            Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                final String next = iterator.next();
                iterator.remove();
            }

            Scanner scanner = new Scanner(System.in);
            final String buttonKey = scanner.next();
            if ("quit".equalsIgnoreCase(buttonKey)) {
                isRunning = false;
                System.out.println("Ладно идите");
            } else {
                control.pressButton(buttonKey);
            }
        }
    }
}
