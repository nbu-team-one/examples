package by.training;

public class ElectricEngine extends Engine {

    public ElectricEngine() {
        super(EngineType.ELECTRIC);
    }

    public static void check() {
        System.out.println("check in electric");
    }

    @Override
    public void start() throws CustomException {

        throw new CustomException("from electric");
    }
}
