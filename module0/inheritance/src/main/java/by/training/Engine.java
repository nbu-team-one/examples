package by.training;

public class Engine {

    private final EngineType engineType;

    public Engine(EngineType engineType) {
        this.engineType = engineType;
    }

    public static void check() {
        System.out.println("check in base");
    }

    public void start() throws Exception {
        System.out.println("I'm base engine");
        throw new Exception("from base");
    }

    public void stop() {

    }
}
