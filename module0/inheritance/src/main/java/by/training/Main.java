package by.training;

public class Main {

    public static void main(String[] args) {

        Engine engine = new Engine(EngineType.BASE);
        Engine electric = new ElectricEngine();

        System.out.println(engine.getClass().getSimpleName());
        System.out.println(electric.getClass().getSimpleName());

        EngineTypeSimple electric1 = EngineTypeSimple.ELECTRIC;
        electric1.start();

        try {
            engine.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            electric.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ((ElectricEngine) electric).start();
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    static Engine createEngine() {
        return new GasEngine();
    }
}
