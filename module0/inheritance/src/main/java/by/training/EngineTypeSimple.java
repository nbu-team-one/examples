package by.training;

public enum EngineTypeSimple {

    ELECTRIC(1) {
        @Override
        void start() {

        }
    },
    BASE(2) {
        @Override
        void start() {

        }
    },
    GASOLINE(3) {
        @Override
        void start() {

        }
    };

    private final int i;

    EngineTypeSimple(int i) {
        this.i = i;
    }

    abstract void start();

    public static EngineTypeSimple fromInt(int i) {
        switch (i) {
            case 1:
                return ELECTRIC;
            case 2:
                return BASE;
            case 3:
                return GASOLINE;
            default:
                return BASE;
        }
    }
}
