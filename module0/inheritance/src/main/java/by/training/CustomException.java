package by.training;

public class CustomException extends Exception {

    public CustomException(String msg) {
        super(msg);
    }
}
