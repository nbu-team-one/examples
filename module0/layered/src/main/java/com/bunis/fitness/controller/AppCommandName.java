package com.bunis.fitness.controller;

public enum AppCommandName {

    SAVE("-s"),
    LOAD("-l"),
    STATISTICS("-S");

    private final String shortCommand;

    AppCommandName(String s) {
        this.shortCommand = s;
    }

    public static AppCommandName fromString(String name) {

        final AppCommandName[] values = AppCommandName.values();
        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                return commandName;
            }
        }
        return null;
    }
}
