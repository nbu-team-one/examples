package com.bunis.fitness.model;

import java.util.Date;
import java.util.Objects;

public class StepData {

    private int steps;
    private Date syncTime;

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public Date getSyncTime() {
        return syncTime;
    }

    public void setSyncTime(Date syncTime) {
        this.syncTime = syncTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StepData stepData = (StepData) o;
        return getSteps() == stepData.getSteps() &&
                getSyncTime().equals(stepData.getSyncTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSteps(), getSyncTime());
    }
}
