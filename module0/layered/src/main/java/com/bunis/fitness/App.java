package com.bunis.fitness;

import com.bunis.fitness.controller.*;
import com.bunis.fitness.service.InMemoryStepDataService;
import com.bunis.fitness.service.SimpleStepStatisticsService;
import com.bunis.fitness.service.StepDataService;
import com.bunis.fitness.service.StepStatisticsService;

import java.util.EnumMap;
import java.util.Map;

public class App {

    public static void main(String[] args) {

        StepDataService dataService = new InMemoryStepDataService();
        StepStatisticsService statisticsService = new SimpleStepStatisticsService();

        AppCommand saveCommand = new StepSavingCommand(dataService);
        AppCommand loadCommand = new StepLoadingCommand(dataService);
        AppCommand statisticCommand = new StatisticCalculationCommand(statisticsService, dataService);
        Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
        commands.put(AppCommandName.LOAD, loadCommand);
        commands.put(AppCommandName.SAVE, saveCommand);
        commands.put(AppCommandName.STATISTICS, statisticCommand);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        // while (isRunning)
        // read command and parameters
        // parse user input
        // controller.handleUserData(Map);

    }
}
