package com.bunis.fitness.model;

import java.util.Objects;

public class StepStatistics {

    private int steps;
    private double calories;

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StepStatistics that = (StepStatistics) o;
        return getSteps() == that.getSteps() &&
                Double.compare(that.getCalories(), getCalories()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSteps(), getCalories());
    }
}
