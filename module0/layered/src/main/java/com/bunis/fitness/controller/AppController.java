package com.bunis.fitness.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(Map<String, String> data) {

        final String commandName = data.get("commandName");


        // close
        try (InputStream is = new BufferedInputStream(null);
             OutputStream out =new BufferedOutputStream(null)) {
            // input stream open

            // stream read
        } catch (Exception e) {

        }
        try {
            final AppCommand command = commandFactory.getCommand(commandName);
            command.execute(data);
        } catch (CommandExecutionException e) {
            // fallback scenario 1

        } catch (CommandFactoryException e) {
            // LOGGER.error(e);
            // fallback scenario 2
            System.out.println("your command is invalid: " + e.getInvalidCommand());
        } catch (CommandException e) {
            // fallback scenario 3
        } catch (Exception el ) {
            // dead
        }

    }
}
