package com.bunis.fitness.controller;

public interface AppCommandFactory {

    AppCommand getCommand(String commandName) throws CommandException;
}
