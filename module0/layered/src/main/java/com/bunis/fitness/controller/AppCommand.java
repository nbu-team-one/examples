package com.bunis.fitness.controller;

import java.util.Map;

public interface AppCommand {

    void execute(Map<String, String> userData);
}
