package com.bunis.fitness.service;

import com.bunis.fitness.model.StepData;

import java.util.Date;
import java.util.List;

public interface StepDataService {

    void saveSteps(StepData steps);

    List<StepData> loadSteps(Date from, Date to);
}
