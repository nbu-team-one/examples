package com.bunis.fitness.controller;

import com.bunis.fitness.model.StepData;
import com.bunis.fitness.model.StepStatistics;
import com.bunis.fitness.service.StepDataService;
import com.bunis.fitness.service.StepStatisticsService;

import java.util.List;
import java.util.Map;

public class StatisticCalculationCommand implements AppCommand {

    private final StepStatisticsService statisticsService;
    private final StepDataService dataService;

    public StatisticCalculationCommand(StepStatisticsService statisticsService, StepDataService dataService) {
        this.statisticsService = statisticsService;
        this.dataService = dataService;
    }

    @Override
    public void execute(Map<String, String> userData) {

        final List<StepData> stepData = dataService.loadSteps(null, null);
        final StepStatistics collect = statisticsService.collect(stepData);
        System.out.println(collect);

    }
}
