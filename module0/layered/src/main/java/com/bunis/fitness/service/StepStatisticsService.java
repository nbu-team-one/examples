package com.bunis.fitness.service;

import com.bunis.fitness.model.StepData;
import com.bunis.fitness.model.StepStatistics;

import java.util.List;

public interface StepStatisticsService {

    StepStatistics collect(List<StepData> steps);
}
