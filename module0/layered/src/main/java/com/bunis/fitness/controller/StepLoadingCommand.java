package com.bunis.fitness.controller;

import com.bunis.fitness.service.StepDataService;

import java.util.Map;

public class StepLoadingCommand implements AppCommand {

    private final StepDataService dataService;

    public StepLoadingCommand(StepDataService dataService) {
        this.dataService = dataService;
    }

    @Override
    public void execute(Map<String, String> userData) {

//        dataService.loadSteps()
    }
}
