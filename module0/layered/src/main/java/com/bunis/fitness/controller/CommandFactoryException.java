package com.bunis.fitness.controller;

public class CommandFactoryException extends CommandException {

    private final String invalidCommand;

    public CommandFactoryException(String command) {
        super("Unable to resolve command: " + command);
        this.invalidCommand = command;
    }

    public String getInvalidCommand() {
        return invalidCommand;
    }
}
