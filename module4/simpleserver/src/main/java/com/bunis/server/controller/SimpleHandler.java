package com.bunis.server.controller;

import com.bunis.server.entity.TrainCar;
import com.bunis.server.service.TrainCarService;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleHandler implements HttpHandler {

    private TrainCarService trainCarService;

    public SimpleHandler(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        try {
            this.handleImpl(exchange);
        } catch (Exception e) {
            // return error code
        }
    }

    private void handleImpl(HttpExchange exchange) throws IOException {

        String requestMethod = exchange.getRequestMethod();
        URI requestURI = exchange.getRequestURI();
        System.out.println("Received incoming request: " + requestMethod + " URI " + requestURI.toString());

        String query = requestURI.getQuery();
        System.out.println("Queried: " + query);

        System.out.println("value from filter:");
        System.out.println(exchange.getAttribute("fromFilter"));

        List<String> inputData = new BufferedReader(new InputStreamReader(exchange.getRequestBody())).lines().collect(Collectors.toList());

        System.out.println("Received:");
        inputData.forEach(System.out::println);
        if (inputData.stream().anyMatch(s -> s.contains("throwError"))) {
            throw new IllegalStateException("As you wish...");
        }

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/view/defaultTemplate.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String template = bufferedReader.lines().collect(Collectors.joining());
        StringBuilder trainCarResponse = new StringBuilder();
        trainCarResponse.append("<ul>");
        trainCarResponse.append("<li>\n" +
                "\t<form action=\"/simple\" method=\"POST\">\n" +
                "\t\t<input type=\"hidden\" name=\"throwError\" value=\"true\">\n" +
                "\t\t<input type=\"submit\" value=\"Throw Error\">\n" +
                "\t</form>\n" +
                "</li>");

        List<TrainCar> trainCars = trainCarService.findAllTrainCars();
        for (TrainCar trainCar : trainCars) {
            trainCarResponse.append("<li>");
            trainCarResponse.append("<form action=\"/simple\" method=\"POST\">\n" +
                    "\t<input type=\"hidden\" name=\"trainCarIdToDelete\" value=\"" + trainCar.getId() + "\"/>\n" +
                    "\t<input type=\"hidden\" name=\"commandName\" value=\"deleteTrainCar\"/>\n" +
                    "\t<input type=\"submit\" value=\"Delete\" />\n" +
                    "</form>");
            trainCarResponse.append(trainCar.toString());
            trainCarResponse.append("</li>");
        }
        trainCarResponse.append("</ul>");

        String view = MessageFormat.format(template, trainCarResponse);
        OutputStream responseBody = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, view.length());
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
    }
}
