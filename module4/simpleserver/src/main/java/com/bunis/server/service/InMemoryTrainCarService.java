package com.bunis.server.service;

import com.bunis.server.entity.TrainCar;
import com.bunis.server.entity.TrainCarType;

import java.util.ArrayList;
import java.util.List;

public class InMemoryTrainCarService implements TrainCarService {

    private List<TrainCar> trainCars = new ArrayList<>(10);

    public InMemoryTrainCarService() {

        for (int i = 0; i < 10; i++) {
            long id = i + 1;
            String name = "TrainCar" + i;
            TrainCarType type = i % 2 == 0 ? TrainCarType.CARGO : TrainCarType.PASSENGER;
            trainCars.add(new TrainCar(id, name, type));
        }
    }

    @Override
    public List<TrainCar> findAllTrainCars() {
        return new ArrayList<>(trainCars);
    }
}
