package com.bunis.server.entity;

public enum TrainCarType {

    PASSENGER,
    CARGO
}
