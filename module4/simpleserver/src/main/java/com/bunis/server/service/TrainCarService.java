package com.bunis.server.service;

import com.bunis.server.entity.TrainCar;

import java.util.List;

public interface TrainCarService {

    List<TrainCar> findAllTrainCars();
}
