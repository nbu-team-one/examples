package com.bunis.server.command;

import java.util.Map;

public interface AppCommand {

    String process(Map<String, String> params);
}
