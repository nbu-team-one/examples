package com.bunis.server;

import com.bunis.server.controller.SimpleHandler;
import com.bunis.server.service.InMemoryTrainCarService;
import com.bunis.server.service.TrainCarService;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerApplication {

    public static void main(String[] args) throws IOException {

        System.setOut(new PrintStream(new File("info_log.txt")));
        System.setErr(new PrintStream(new File("error_log.txt")));

        InetSocketAddress localhost = new InetSocketAddress("localhost", 49090);
        HttpServer server = HttpServer.create(localhost, 0);

        // init DAO
        // init Service
        TrainCarService trainCarService = new InMemoryTrainCarService();

        // init Commands


        HttpContext serverContext = server.createContext("/simple", new SimpleHandler(trainCarService));
        serverContext.getFilters().add(new Filter() {
            @Override
            public void doFilter(HttpExchange exchange, Chain chain) throws IOException {

                System.out.println("Hello from filter");
                exchange.setAttribute("fromFilter", "filterValue");
                chain.doFilter(exchange);
            }

            @Override
            public String description() {
                return "Simple Filter";
            }
        });

        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();

        System.out.println("Server started on " + server.getAddress().toString());
    }
}
