package com.bunis.server.entity;

public class TrainCar {

    private Long id;
    private String name;
    private TrainCarType type;

    public TrainCar(Long id, String name, TrainCarType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TrainCarType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TrainCar{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
