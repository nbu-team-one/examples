insert into
    user_account
    (login, password, registration_date, email, first_name, last_name)
    values
    ('admin', 'admin', '2019-11-25 14:00:00 +02:00', 'admin@email', 'admin', 'admin');

insert into
    user_role
    ('technical_name', 'display_name')
    values ('admin', 'Super User');

insert into
    user_role_rel
	(user_id, role_id)
    select u_.id, r_.id  from
	user_account u_,
	user_role r_
	where u_.login = 'admin'
	and
	r_.technical_name = 'admin';