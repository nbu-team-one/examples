CREATE TABLE user_account
(
    id bigserial NOT NULL,
    login character(300) NOT NULL,
    password character(300) NOT NULL,
    registration_date timestamp with time zone NOT NULL,
    email character(300) NOT NULL,
    first_name character(300) NOT NULL,
    last_name character(300) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT unique_login UNIQUE (login),
    CONSTRAINT unique_email UNIQUE (email)
);

CREATE TABLE user_role
(
    id bigserial NOT NULL,
    technical_name character(300) NOT NULL,
    display_name character(300) NOT NULL,
    description character(1000),
    is_default boolean default false,
    PRIMARY KEY (id),
    CONSTRAINT unique_role UNIQUE (role_name)
);

CREATE TABLE user_role_rel
(
    user_id bigserial NOT NULL,
    role_id bigserial NOT NULL,
    id bigserial NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT user_id_ref FOREIGN KEY (user_id)
        REFERENCES user_account (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT role_id_ref FOREIGN KEY (role_id)
        REFERENCES user_role (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);