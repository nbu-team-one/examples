-- SEQUENCE: public.scooter_pk

-- DROP SEQUENCE public.scooter_pk;

CREATE SEQUENCE public.scooter_pk;

ALTER SEQUENCE public.scooter_pk
    OWNER TO postgres;

GRANT ALL ON SEQUENCE public.scooter_pk TO scooter_admin;

GRANT ALL ON SEQUENCE public.scooter_pk TO postgres;