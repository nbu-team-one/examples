<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col">
    <c:choose>
        <c:when test="${not empty users}">
            <table class="mdl-data-table mdl-js-data-table">
                  <thead>
                    <tr>
                      <th class="mdl-data-table__cell--non-numeric">Name</th>
                      <th class="mdl-data-table__cell--non-numeric">Last name</th>
                      <th class="mdl-data-table__cell--non-numeric">Email</th>
                      <th class="mdl-data-table__cell--non-numeric">Login</th>
                      <th class="mdl-data-table__cell--non-numeric" colspan=3>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:forEach items="${users}" var="u">
                        <tr>
                          <td class="mdl-data-table__cell--non-numeric">${u.firstName}</td>
                          <td class="mdl-data-table__cell--non-numeric">${u.lastName}</td>
                          <td class="mdl-data-table__cell--non-numeric">${u.email}</td>
                          <td class="mdl-data-table__cell--non-numeric">${u.login}</td>

                          <td class="mdl-data-table__cell--non-numeric">
                            <form action="${pageContext.request.contextPath}/" method="POST">
                                <input type="hidden" name="userId" value="${u.id}"/>
                                <input type="hidden" name="commandName" value="editUser"/>
                                <input class="mdl-button mdl-js-button" type="submit" value="Edit user"/>
                            </form>
                          </td>
                          <td class="mdl-data-table__cell--non-numeric">
                            <form action="${pageContext.request.contextPath}/" method="POST">
                                <input type="hidden" name="userId" value="${u.id}"/>
                                <input type="hidden" name="commandName" value="lockUser"/>
                                <input class="mdl-button mdl-js-button" type="submit" value="Lock user"/>
                            </form>
                          </td>
                          <td class="mdl-data-table__cell--non-numeric">
                            <form action="${pageContext.request.contextPath}/" method="POST">
                                <input type="hidden" name="userId" value="${u.id}"/>
                                <input type="hidden" name="commandName" value="deleteUser"/>
                                <input class="mdl-button mdl-js-button" type="submit" value="Delete user"/>
                            </form>
                          </td>
                        </tr>
                    </c:forEach>
                  </tbody>
                </table>
        </c:when>
        <c:otherwise>
        no data available
        </c:otherwise>
    </c:choose>

  </div>
</div>