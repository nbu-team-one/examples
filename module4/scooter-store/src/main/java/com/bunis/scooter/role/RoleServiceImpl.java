package com.bunis.scooter.role;

import com.bunis.scooter.core.Bean;
import com.bunis.scooter.dao.TransactionSupport;
import com.bunis.scooter.dao.Transactional;
import com.bunis.scooter.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Bean
@Log4j
@TransactionSupport
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private RoleDao roleDao;

    @Override
    public List<RoleDto> getUserRoles(UserDto user) {
        return roleDao.getUserRoles(user.getId());
    }

    @Override
    @Transactional
    public void assignRoles(UserDto user, List<RoleDto> roles) {

        try {
            for (RoleDto role : roles) {
                roleDao.assignRole(user.getId(), role.getId());
            }
        } catch (SQLException e) {
            log.error("Failed to assign roles", e);
        }
    }

    @Override
    public void assignDefaultRoles(Long userId) {

        try {
            roleDao.assignDefaultRoles(userId);
        } catch (SQLException e) {
            log.error("Failed to assign default roles", e);
        }
    }

    @Override
    public List<RoleDto> findAllRoles() {
        try {
            return roleDao.findAll();
        } catch (SQLException e) {
            log.error("Failed to read all roles", e);
            return new ArrayList<>();
        }
    }

    @Override
    public boolean saveRole(RoleDto role) {
        try {
            return roleDao.save(role) != null;
        } catch (SQLException e) {
            log.error("Failed to save role", e);
            return false;
        }
    }

    @Override
    public RoleDto getRole(Long id) {
        try {
            return roleDao.getById(id);
        } catch (SQLException e) {
            log.error("Failed to read role:" + id, e);
            return null;
        }
    }
}
