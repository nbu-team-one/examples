package com.bunis.scooter.user;

import com.bunis.scooter.command.CommandException;
import com.bunis.scooter.command.ServletCommand;
import com.bunis.scooter.core.Bean;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.bunis.scooter.ApplicationConstants.VIEWNAME_REQ_PARAMETER;
import static com.bunis.scooter.ApplicationConstants.VIEW_ALL_USERS_CMD_NAME;

@Log4j
@Bean(name = VIEW_ALL_USERS_CMD_NAME)
public class ViewUserListCommand implements ServletCommand {

    private UserService userService;

    public ViewUserListCommand(UserService userService) {

        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        final List<UserDto> allUsers = userService.getAllUsers();
        req.setAttribute("users", allUsers);

        try {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ALL_USERS_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            log.error("Something went wrong...", e);
            throw new CommandException(e);
        }
    }
}
