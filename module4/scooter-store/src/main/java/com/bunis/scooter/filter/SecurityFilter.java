package com.bunis.scooter.filter;

import com.bunis.scooter.ApplicationConstants;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.bunis.scooter.ApplicationConstants.CMD_REQ_PARAMETER;

//@WebFilter(servletNames = "app", filterName = "SecurityFilter")
public class SecurityFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        final String parameter = request.getParameter(CMD_REQ_PARAMETER);
        if (parameter == null || parameter.length() == 0) {
            chain.doFilter(request, response);
        } else if (ApplicationConstants.ALL_COMMANDS.contains(parameter)) {

        } else {
            ((HttpServletResponse) response).sendRedirect("/");
        }
    }

    @Override
    public void destroy() {

    }
}
