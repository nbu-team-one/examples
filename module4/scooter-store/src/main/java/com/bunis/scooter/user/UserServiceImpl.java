package com.bunis.scooter.user;

import com.bunis.scooter.core.Bean;
import com.bunis.scooter.dao.TransactionSupport;
import com.bunis.scooter.dao.Transactional;
import com.bunis.scooter.role.RoleService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
@Bean
@AllArgsConstructor
@TransactionSupport
public class UserServiceImpl implements UserService {

    private UserDao userDao;
    private RoleService roleService;

    @Override
    public boolean loginUser(UserDto userDto) {

        Optional<UserDto> byLogin;
        try {
            byLogin = userDao.findByLogin(userDto.getLogin());
        } catch (SQLException e) {
            log.error("Failed to read user", e);
            byLogin = Optional.empty();
        }
        return byLogin.filter(dto -> dto.getPassword().equals(userDto.getPassword())).isPresent();
    }

    @Override
    @Transactional
    public boolean registerUser(UserDto userDto) {

        try {
            Long saved = userDao.save(userDto);
            roleService.assignDefaultRoles(saved);
            return true;
        } catch (SQLException e) {
            log.error("Failed to save user", e);
            return false;
        }
    }

    @Override
    public List<UserDto> getAllUsers() {
        try {
            return userDao.findAll();
        } catch (SQLException e) {
            log.error("Failed to read users", e);
            return new ArrayList<>();
        }
    }
}