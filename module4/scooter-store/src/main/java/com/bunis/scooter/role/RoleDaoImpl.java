package com.bunis.scooter.role;

import com.bunis.scooter.core.Bean;
import com.bunis.scooter.dao.ConnectionManager;
import lombok.AllArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
@AllArgsConstructor
public class RoleDaoImpl implements RoleDao {

    private static final String SELECT_DEFAULT_ROLES = "select role_.id as role_id from user_roles role_ where role_.default_role = true";
    private static final String ASSIGN_ROLE = "insert into user_role_relation (user_id, role_id) values (?, ?);";


    private ConnectionManager connectionManager;

    @Override
    public void assignDefaultRoles(Long userId) throws SQLException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement stmt = connection.prepareStatement(SELECT_DEFAULT_ROLES)) {
            final ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                final long roleId = resultSet.getLong(1);
                assignRole(roleId, userId, connection);
            }
        }

    }

    @Override
    public void assignRole(Long roleId, Long userId) throws SQLException {

        this.assignRole(roleId, userId, connectionManager.getConnection());
    }

    private void assignRole(Long roleId, Long userId, Connection connection) throws SQLException {

        try (PreparedStatement stmt = connection.prepareStatement(ASSIGN_ROLE)) {
            int i = 0;
            stmt.setLong(++i, userId);
            stmt.setLong(++i, roleId);
            stmt.executeUpdate();
        }
    }

    @Override
    public List<RoleDto> getUserRoles(Long userId) {
        return null;
    }

    @Override
    public Long save(RoleDto roleDto) throws SQLException {
        return null;
    }

    @Override
    public boolean update(RoleDto roleDto) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(RoleDto roleDto) throws SQLException {
        return false;
    }

    @Override
    public RoleDto getById(Long id) throws SQLException {
        return null;
    }

    @Override
    public List<RoleDto> findAll() throws SQLException {
        return null;
    }
}
