package com.bunis.scooter.user;

import com.bunis.scooter.command.CommandException;
import com.bunis.scooter.command.ServletCommand;
import com.bunis.scooter.core.Bean;
import com.bunis.scooter.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.bunis.scooter.ApplicationConstants.LOGIN_CMD_NAME;


@Bean(name = LOGIN_CMD_NAME)
public class LoginUserCommand implements ServletCommand {

    private UserService userService;

    public LoginUserCommand(UserService service) {

        this.userService = service;
    }

    public UserService getUserService() {

        return userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            resp.getWriter().write("Executed in LoginUserCommand");
        } catch (IOException e) {
            throw new CommandException(e);
        }
    }
}
