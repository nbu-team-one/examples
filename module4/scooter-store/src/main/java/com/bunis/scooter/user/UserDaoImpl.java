package com.bunis.scooter.user;

import com.bunis.scooter.core.Bean;
import com.bunis.scooter.dao.ConnectionManager;
import com.bunis.scooter.entity.UserEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Bean
public class UserDaoImpl implements UserDao {

    private final static AtomicLong COUNTER = new AtomicLong(1);

    private static final String SELECT_ALL_QUERY = "select id, login, password, email, first_name, last_name from user_account";
    private static final String SELECT_BY_ID_QUERY = "select id, login, password, email, first_name, last_name from user_account where id = ?";
    private static final String SELECT_BY_LOGIN_QUERY = "select id, login, password, email, first_name, last_name from user_account where login = ?";
    private static final String INSERT_QUERY = "insert into user_account (id, login, password, email, first_name, last_name) values (?,?,?,?,?,?)";
    private static final String UPDATE_QUERY = "update user_account set login=?, password=?, email=?, first_name=?, last_name=? where id = ?";
    private static final String DELETE_QUERY = "delete from user_account where id = ?";

    private ConnectionManager connectionManager;

    public UserDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(UserDto userDto) throws SQLException {

        UserEntity entity = fromDto(userDto);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setLong(++i, COUNTER.getAndIncrement());
            insertStmt.setString(++i, entity.getLogin());
            insertStmt.setString(++i, entity.getPassword());
            insertStmt.setString(++i, entity.getEmail());
            insertStmt.setString(++i, entity.getFirstName());
            insertStmt.setString(++i, entity.getLastName());
            insertStmt.executeUpdate();
            ResultSet generatedKeys = insertStmt.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        }

        return entity.getId();
    }

    @Override
    public boolean update(UserDto userDto) throws SQLException {

        UserEntity entity = fromDto(userDto);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            int i = 0;
            updateStmt.setString(++i, entity.getLogin());
            updateStmt.setString(++i, entity.getPassword());
            updateStmt.setString(++i, entity.getEmail());
            updateStmt.setString(++i, entity.getFirstName());
            updateStmt.setString(++i, entity.getLastName());
            updateStmt.setLong(++i, entity.getId());
            return updateStmt.executeUpdate() > 0;
        }
    }

    @Override
    public boolean delete(UserDto userDto) throws SQLException {
        UserEntity entity = fromDto(userDto);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, entity.getId());
            return updateStmt.executeUpdate() > 0;
        }
    }

    @Override
    public UserDto getById(Long id) throws SQLException {

        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }

        return result.stream().map(this::fromEntity).findFirst().orElseThrow(() -> new IllegalArgumentException("Entity not found with given id: " + id));
    }

    @Override
    public List<UserDto> findAll() throws SQLException {
        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }
        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<UserDto> findByLogin(String login) throws SQLException {

        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_LOGIN_QUERY)) {
            selectStmt.setString(1, login);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }

        return result.stream().map(this::fromEntity).findFirst();
    }

    private UserEntity parseResultSet(ResultSet resultSet) throws SQLException {
        long entityId = resultSet.getLong("id");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String email = resultSet.getString("email");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        return UserEntity.builder()
                .id(entityId)
                .login(login)
                .password(password)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }

    private UserEntity fromDto(UserDto dto) {

        UserEntity entity = new UserEntity();
        entity.setEmail(dto.getEmail());
        entity.setFirstName(dto.getFirstName());
        entity.setId(dto.getId());
        entity.setLastName(dto.getLastName());
        entity.setLogin(dto.getLogin());
        entity.setPassword(dto.getPassword());

        return entity;
    }

    private UserDto fromEntity(UserEntity entity) {

        UserDto dto = new UserDto();
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setId(entity.getId());
        dto.setLastName(entity.getLastName());
        dto.setLogin(entity.getLogin());
        dto.setPassword(entity.getPassword());

        return dto;
    }
}
