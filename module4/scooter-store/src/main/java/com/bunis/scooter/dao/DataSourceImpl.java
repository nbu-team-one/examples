package com.bunis.scooter.dao;

import com.bunis.scooter.core.Bean;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

@Bean
public class DataSourceImpl implements DataSource {

    private final HikariDataSource ds;

    public DataSourceImpl() {
        HikariConfig config = new HikariConfig("/datasource.properties");
        ds = new HikariDataSource(config);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    @Override
    public void close() {
        ds.close();
    }
}
