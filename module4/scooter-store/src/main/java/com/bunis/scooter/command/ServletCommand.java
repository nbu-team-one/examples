package com.bunis.scooter.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@FunctionalInterface
public interface ServletCommand {

    void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException;
}