package com.bunis.scooter.user;

import com.bunis.scooter.ApplicationConstants;
import com.bunis.scooter.command.CommandException;
import com.bunis.scooter.command.ServletCommand;
import com.bunis.scooter.core.Bean;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.bunis.scooter.ApplicationConstants.REGISTER_SAVE_CMD_NAME;
import static com.bunis.scooter.ApplicationConstants.REGISTER_VIEW_CMD_NAME;

@Log4j
@Bean(name = REGISTER_SAVE_CMD_NAME)
@AllArgsConstructor
public class RegisterSaveUserCommand implements ServletCommand {

    private UserService userService;


    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        final String login = req.getParameter("user.login");
        final String firstName = req.getParameter("user.firstName");
        final String lastName = req.getParameter("user.lastName");
        final String email = req.getParameter("user.email");
        final String password = req.getParameter("user.password");
        UserDto dto = new UserDto();
        dto.setPassword(password);
        dto.setLogin(login);
        dto.setEmail(email);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);

        final boolean saved = userService.registerUser(dto);
        if (saved) {
            try {
                resp.sendRedirect("/?commandName=" + ApplicationConstants.VIEW_ALL_USERS_CMD_NAME);
            } catch (IOException e) {
                throw new CommandException("Failed to forward", e);
            }
        } else {
            req.setAttribute("user", dto);
            req.setAttribute("viewName", REGISTER_VIEW_CMD_NAME);
            try {
                req.getRequestDispatcher("jsp/layout.jsp").forward(req, resp);
            } catch (ServletException | IOException e) {
                throw new CommandException("Failed to forward", e);
            }
        }
    }
}
