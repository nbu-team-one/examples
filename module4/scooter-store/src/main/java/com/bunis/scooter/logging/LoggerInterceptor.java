package com.bunis.scooter.logging;

import com.bunis.scooter.core.Bean;
import com.bunis.scooter.core.BeanInterceptor;
import com.bunis.scooter.core.Interceptor;
import lombok.extern.log4j.Log4j;

import java.lang.reflect.Method;

@Log4j
@Bean(name = "loggerInt")
@Interceptor(clazz = LoggingSupport.class)
public class LoggerInterceptor implements BeanInterceptor {


    @Override
    public void before(Object proxy, Object service, Method method, Object[] args) {
        log.info("Before");
    }

    @Override
    public void success(Object proxy, Object service, Method method, Object[] args) {

    }

    @Override
    public void fail(Object proxy, Object service, Method method, Object[] args) {
        log.error("");
    }
}
