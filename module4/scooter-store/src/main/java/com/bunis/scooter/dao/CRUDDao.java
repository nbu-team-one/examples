package com.bunis.scooter.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Predicate;

public interface CRUDDao<ENTITY, KEY> {

    KEY save(ENTITY entity) throws SQLException;

    boolean update(ENTITY entity) throws SQLException;

    boolean delete(ENTITY entity) throws SQLException;

    ENTITY getById(KEY id) throws SQLException;

    List<ENTITY> findAll() throws SQLException;
}
