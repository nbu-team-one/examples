package com.bunis.scooter.role;

import com.bunis.scooter.user.UserDto;

import java.util.List;

public interface RoleService {

    List<RoleDto> getUserRoles(UserDto user);

    void assignRoles(UserDto user, List<RoleDto> roles);

    void assignDefaultRoles(Long userId);

    List<RoleDto> findAllRoles();

    boolean saveRole(RoleDto role);

    RoleDto getRole(Long id);
}
