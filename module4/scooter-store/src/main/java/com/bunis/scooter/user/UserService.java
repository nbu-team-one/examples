package com.bunis.scooter.user;

import java.util.List;

public interface UserService {

    boolean loginUser(UserDto userDto);

    boolean registerUser(UserDto userDto);

    List<UserDto> getAllUsers();
}