package com.bunis.scooter.scooter;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ScooterDto {

    private Long id;
    private String model;
    private String description;
    private Double energyCapacity;
    private Double price;
}