package com.bunis.scooter.user;

import com.bunis.scooter.command.CommandException;
import com.bunis.scooter.command.ServletCommand;
import com.bunis.scooter.core.Bean;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.bunis.scooter.ApplicationConstants.REGISTER_VIEW_CMD_NAME;
import static com.bunis.scooter.ApplicationConstants.VIEWNAME_REQ_PARAMETER;

@Log4j
@Bean(name = REGISTER_VIEW_CMD_NAME)
public class RegisterViewUserCommand implements ServletCommand {

    private UserService userService;

    public RegisterViewUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

        try {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            log.error("Something went wrong...", e);
            throw new CommandException(e);
        }
    }

    public UserService getUserService() {
        return userService;
    }
}
