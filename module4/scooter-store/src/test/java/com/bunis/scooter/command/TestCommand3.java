package com.bunis.scooter.command;

import com.bunis.scooter.core.Bean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = "test3")
public class TestCommand3 implements ServletCommand {
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {

    }
}
