package com.bunis.scooter.command;

import com.bunis.scooter.core.Bean;
import com.bunis.scooter.service.ProxyService;
import lombok.AllArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@AllArgsConstructor
@Bean(name = "proxyCommand")
public class TestCommandWithProxy implements ServletCommand {

    private ProxyService service;

    public ProxyService getService() {
        return service;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {

    }
}
