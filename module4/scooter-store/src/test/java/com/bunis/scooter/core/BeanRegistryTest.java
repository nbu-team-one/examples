package com.bunis.scooter.core;

import com.bunis.scooter.dao.ConnectionManagerImpl;
import com.bunis.scooter.user.*;
import com.bunis.scooter.command.ServletCommand;
import com.bunis.scooter.command.TestCommand1;
import com.bunis.scooter.command.TestCommand2;
import com.bunis.scooter.command.TestCommand3;
import com.bunis.scooter.command.TestCommandWithProxy;
import com.bunis.scooter.service.ProxyServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class BeanRegistryTest {

    @Test
    public void shouldRegisterAndReturnServiceByInterface() {

        BeanRegistry provider = new BeanRegistryImpl();
        UserService serviceImpl = new UserServiceImpl(null, null);
        provider.registerBean(serviceImpl);
        UserService serviceFound = provider.getBean(UserService.class);
        Assert.assertEquals(serviceImpl, serviceFound);
    }

    @Test
    public void shouldRegisterAndReturnServiceByName() {

        BeanRegistry provider = new BeanRegistryImpl();
        UserService serviceImpl = new UserServiceImpl(null, null);
        provider.registerBean(serviceImpl);
        UserService serviceFound = provider.getBean("UserService");
        Assert.assertEquals(serviceImpl, serviceFound);
    }

    @Test
    public void shouldRegisterAndInjectAndReturnNewServiceByName() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(UserServiceImpl.class);
        provider.registerBean(LoginUserCommand.class);
        ServletCommand serviceFound = provider.getBean("loginUser");
        Assert.assertNotNull(serviceFound);
        Assert.assertTrue(serviceFound instanceof LoginUserCommand);
        UserService userService = ((LoginUserCommand) serviceFound).getUserService();
        Assert.assertNotNull(userService);
    }

    @Test
    public void shouldRegisterAndInjectAndReturnNewServiceByClass() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(RegisterViewUserCommand.class);
        provider.registerBean(UserServiceImpl.class);
        RegisterViewUserCommand serviceFound = provider.getBean(RegisterViewUserCommand.class);
        Assert.assertNotNull(serviceFound);
        UserService userService = serviceFound.getUserService();
        Assert.assertNotNull(userService);
    }

    @Test
    public void shouldRegisterAndInjectSameAndReturnNewServiceByClass() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(RegisterViewUserCommand.class);
        provider.registerBean(UserServiceImpl.class);
        provider.registerBean(UserDaoImpl.class);
        provider.registerBean(ConnectionManagerImpl.class);
        UserService firstService = provider.getBean("UserService");
        RegisterViewUserCommand serviceFound = provider.getBean(RegisterViewUserCommand.class);
        Assert.assertNotNull(serviceFound);
        UserService userService = serviceFound.getUserService();
        Assert.assertEquals(firstService, userService);
    }

    @Test(expected = NotUniqueBeanException.class)
    public void shouldThrowExceptionOnDuplicateName() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(TestCommand1.class);
        provider.registerBean(TestCommand2.class);
    }

    @Test()
    public void shouldRegisterMultipleImplementations() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(TestCommand1.class);
        provider.registerBean(TestCommand3.class);
    }

    @Test
    public void shouldRegisterAndInjectProxy() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(ProxyServiceImpl.class);
        provider.registerBean(TestCommandWithProxy.class);
        ServletCommand proxyCommand = provider.getBean("proxyCommand");
        Assert.assertTrue(proxyCommand instanceof TestCommandWithProxy);
    }

}
