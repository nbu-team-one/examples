package com.bunis.scooter.dao;

import com.bunis.scooter.ApplicationContext;
import com.bunis.scooter.user.UserDto;
import com.bunis.scooter.user.UserService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@RunWith(JUnit4.class)
public class UserDaoTest {

    @Before
    public void createTable() throws SQLException {

        ApplicationContext.initialize();
        String sql = "CREATE TABLE user_account (\n" +
                "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 100, INCREMENT BY 1) PRIMARY KEY,\n" +
                "  login VARCHAR(30),\n" +
                "  password VARCHAR(30),\n" +
                "  email  VARCHAR(50),\n" +
                "  first_name  VARCHAR(50),\n" +
                "  last_name  VARCHAR(50)\n" +
                ")";
        executeSql(sql);
    }

    @After
    public void dropTable() throws SQLException {
        String sql = "DROP TABLE user_account";
        executeSql(sql);
        ApplicationContext.getInstance().destroy();
    }

    @Test
    public void shouldRegisterUserInNewTransaction() throws SQLException {

        TransactionManagerImpl spyTransactionManager = reInitBeans();

        UserService userService = ApplicationContext.getInstance().getBean(UserService.class);
        Assert.assertNotNull(userService);

        UserDto dto = new UserDto();
        dto.setLogin("test1");
        dto.setPassword("test1");
        boolean registered = userService.registerUser(dto);
        Assert.assertTrue(registered);

        List<UserDto> allUsers = userService.getAllUsers();
        Assert.assertEquals(1, allUsers.size());
        Mockito.verify(spyTransactionManager, Mockito.times(1)).beginTransaction();
        Mockito.verify(spyTransactionManager, Mockito.times(1)).commitTransaction();
    }

    private TransactionManagerImpl reInitBeans() {
        DataSource dataSource = ApplicationContext.getInstance().getBean(DataSource.class);
        TransactionManager transactionManager = ApplicationContext.getInstance().getBean(TransactionManager.class);
        ConnectionManagerImpl connectionManager = ApplicationContext.getInstance().getBean(ConnectionManagerImpl.class);
        TransactionInterceptor transactionInterceptor = ApplicationContext.getInstance().getBean(TransactionInterceptor.class);
        boolean removedTransactionManager = ApplicationContext.getInstance().removeBean(transactionManager);
        boolean removedConnectionManager = ApplicationContext.getInstance().removeBean(connectionManager);
        boolean removedTransactionInterceptor = ApplicationContext.getInstance().removeBean(transactionInterceptor);

        Assert.assertTrue(removedTransactionManager);
        Assert.assertTrue(removedConnectionManager);
        Assert.assertTrue(removedTransactionInterceptor);

        TransactionManagerImpl spyTransactionManager = Mockito.spy(new TransactionManagerImpl(dataSource));
        ConnectionManagerImpl spyConnectionManager = new ConnectionManagerImpl(spyTransactionManager, dataSource);
        TransactionInterceptor spyTransactionInterceptor = new TransactionInterceptor(spyTransactionManager);
        ApplicationContext.getInstance().registerBean(spyTransactionManager);
        ApplicationContext.getInstance().registerBean(spyConnectionManager);
        ApplicationContext.getInstance().registerBean(spyTransactionInterceptor);
        return spyTransactionManager;
    }

    private void executeSql(String sql) throws SQLException {
        DataSource dataSource = new DataSourceImpl();
        Connection connection = dataSource.getConnection();
        PreparedStatement createTableStatement = connection.prepareStatement(sql);
        createTableStatement.executeUpdate();
        createTableStatement.close();
        connection.close();
    }

}
