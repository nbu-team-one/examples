package com.bunis.um.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class UserDto implements Serializable {

    private static final long serialVersionUID = 497371017619545748L;
    private Long id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("login_name")
    private String loginName;
    private String email;
    private String password;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("ip_address")
    private String ipAddress;
    @JsonProperty("role")
    private String role;

}
