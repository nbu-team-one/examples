package com.bunis.um.command;

import com.bunis.um.SecurityContext;
import com.bunis.um.dto.UserDto;
import com.bunis.um.service.UserServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class LoginSaveCommand implements Command {
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String login = request.getParameter("user.loginName");
        List<UserDto> userDtos = UserServiceFactory.getUserService().find(u -> u.getLoginName().equalsIgnoreCase(login));
        if (userDtos.size() != 1) {
            request.setAttribute("error", "login invalid");
            return "login";
        }

        UserDto userDto = userDtos.get(0);

        SecurityContext.getInstance().login(userDto, request.getSession().getId());

        return "redirect:?_command=" + CommandType.INDEX;
    }
}
