package com.bunis.um.controller;

import com.bunis.um.command.Command;
import com.bunis.um.command.CommandFactory;
import com.bunis.um.command.CommandType;
import com.bunis.um.command.CommandUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;

@WebServlet(urlPatterns = "/", name = "index")
public class IndexController extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 6154677369722697748L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String commandFromRequest = CommandUtil.getCommandFromRequest(req);

        Command command = CommandFactory.getCommand(commandFromRequest);
        String viewName = command.apply(req, resp);
        if (viewName.startsWith("redirect:")) {
            String redirect = viewName.replace("redirect:", "");
            resp.sendRedirect(redirect);
        } else {
            req.setAttribute("viewName", viewName);
            req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        doGet(req, resp);
    }
}
