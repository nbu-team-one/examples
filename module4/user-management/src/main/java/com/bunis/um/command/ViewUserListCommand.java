package com.bunis.um.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bunis.um.dto.UserDto;
import com.bunis.um.service.UserServiceFactory;

public class ViewUserListCommand implements Command {

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<UserDto> all = UserServiceFactory.getUserService().findAll();
        request.setAttribute("userList", all);
        return "user_list";
    }
}
