package com.bunis.um.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bunis.um.dto.UserDto;
import com.bunis.um.service.UserService;
import com.bunis.um.service.UserServiceFactory;

public class DeleteUserCommand implements Command {

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long userId = Long.valueOf(request.getParameter("user.id"));

        UserService userService = UserServiceFactory.getUserService();
        UserDto byId = userService.getById(userId);
        userService.delete(byId);
        return "redirect:?_command=" + CommandType.VIEW_USER_LIST.name();
    }
}
