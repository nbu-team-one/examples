package com.bunis.um.command;

import com.bunis.um.dto.UserDto;
import com.bunis.um.service.UserService;
import com.bunis.um.service.UserServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditUserSaveCommand implements Command {
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserService userService = UserServiceFactory.getUserService();
        String userIdParameter = request.getParameter("user.id");
        Long id = Long.parseLong(userIdParameter);

        String loginNameParameter = request.getParameter("user.loginName");
        // validate id & loginName

        if (loginNameParameter == null || loginNameParameter.length() == 0) {

            UserDto byId = userService.getById(id);

            request.setAttribute("user", byId);
            request.setAttribute("errorMsg", "Login Name is empty");
            return "edit_user";
        }




        UserDto entity = userService.getById(id);
        // check does entity exist
        //
        entity.setLoginName(loginNameParameter);
        userService.save(entity);

        return "redirect:?_command=" + CommandType.VIEW_USER_DETAILS.name() + "&id=" + userIdParameter;
    }
}
