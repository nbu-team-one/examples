package com.bunis.um.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bunis.um.dto.UserDto;
import com.bunis.um.service.UserServiceFactory;

public class ViewUserDetail implements Command {

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long id = Long.valueOf(request.getParameter("id"));
        UserDto byId = UserServiceFactory.getUserService().getById(id);
        request.setAttribute("user", byId);
        return "user_detail";
    }
}
