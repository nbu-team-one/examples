package com.bunis.um.command;

import com.bunis.um.dto.UserDto;
import com.bunis.um.service.UserService;
import com.bunis.um.service.UserServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditUserViewCommand implements Command {

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserService userService = UserServiceFactory.getUserService();
        Long userId = Long.valueOf(request.getParameter("user.id"));

        UserDto byId = userService.getById(userId);

        request.setAttribute("user", byId);
        return "edit_user";
    }
}
