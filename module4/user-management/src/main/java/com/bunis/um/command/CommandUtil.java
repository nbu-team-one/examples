package com.bunis.um.command;

import javax.servlet.http.HttpServletRequest;

import com.bunis.um.ApplicationModule;

public class CommandUtil {

    public static String getCommandFromRequest(HttpServletRequest request) {

        return request.getAttribute(ApplicationModule.COMMAND_PARAM) != null ? String.valueOf(request.getAttribute(
                ApplicationModule.COMMAND_PARAM)) :
                request.getParameter(ApplicationModule.COMMAND_PARAM);
    }
}
