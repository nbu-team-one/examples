package com.bunis.um.service;

import java.util.List;
import java.util.function.Predicate;

import com.bunis.um.dto.UserDto;

public interface UserService {

    UserDto getById(Long id);

    List<UserDto> find(Predicate<UserDto> criteria);

    List<UserDto> findAll();

    void delete(UserDto dto);

    void save(UserDto dto);
}
