package com.bunis.um.dto;

public enum UserRole {

    ADMIN,
    MANAGER,
    CLIENT
}
