package com.bunis.um.service;

public class UserServiceFactory {

    private static final UserServiceImpl USER_SERVICE = new UserServiceImpl();

    public static UserService getUserService() {

        return USER_SERVICE;
    }
}
