package com.bunis.um.command;

import com.bunis.um.SecurityContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CommandFactory {

    private static final Map<CommandType, Command> commands = new ConcurrentHashMap<>(CommandType.values().length);

    static {
        commands.put(CommandType.VIEW_USER_LIST, new ViewUserListCommand());
        commands.put(CommandType.VIEW_USER_DETAILS, new ViewUserDetail());
        commands.put(CommandType.CREATE_USER, (request, response) -> "create");
        commands.put(CommandType.EDIT_VIEW_USER, new EditUserViewCommand());
        commands.put(CommandType.EDIT_SAVE_USER, new EditUserSaveCommand());
        commands.put(CommandType.DELETE_USER, new DeleteUserCommand());
        commands.put(CommandType.LOGIN_DISPLAY, (request, response) -> "login");
        commands.put(CommandType.LOGIN_SUBMIT, new LoginSaveCommand());
        commands.put(CommandType.LOGOUT, (request, response) -> {

            request.getSession().invalidate();
            SecurityContext.getInstance().logout(request.getSession().getId());
            return "redirect:?_command=" + CommandType.INDEX;
        });
        commands.put(CommandType.INDEX, (request, response) -> "index");

    }

    public static Command getCommand(String commandParam) {

        return CommandType.of(commandParam).map(commands::get).orElse(((request, response) -> "index"));
    }

}
