package com.bunis.um.service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.bunis.um.dto.UserDto;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserServiceImpl implements UserService {

    private static final List<UserDto> users = new ArrayList<>(100);
    private static final AtomicLong ID_COUNTER = new AtomicLong(101);
    private static final Lock USER_LOCK = new ReentrantLock();

    static {
        URL resource = UserServiceImpl.class.getResource("/MOCK_DATA.json");

        ObjectMapper mapper = new ObjectMapper();
        try {
            List<UserDto> parsed = mapper.readValue(resource, new TypeReference<List<UserDto>>() {
            });
            users.addAll(parsed.subList(0, 20));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public UserDto getById(Long id) {

        return searchStream(dto -> dto.getId().equals(id)).findFirst()
                .orElseThrow(() -> new UserNotFoundException("Cannot find user with id" + id));
    }

    @Override
    public List<UserDto> find(Predicate<UserDto> criteria) {

        return searchStream(criteria).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> findAll() {

        return users;
    }

    @Override
    public void delete(UserDto dto) {

        USER_LOCK.lock();
        try {
            users.remove(dto);
        } finally {
            USER_LOCK.unlock();
        }
    }

    @Override
    public void save(UserDto dto) {

        USER_LOCK.lock();
        try {

            if (dto.getId() != null) {

                UserDto existing = getById(dto.getId());
                existing.setEmail(dto.getEmail());
                existing.setFirstName(dto.getFirstName());
                existing.setIpAddress(dto.getIpAddress());
                existing.setLastName(dto.getLastName());
                existing.setLoginName(dto.getLoginName());
                existing.setPassword(dto.getPassword());
                existing.setPhoneNumber(dto.getPhoneNumber());
            } else {
                dto.setId(ID_COUNTER.getAndIncrement());
                users.add(dto);
            }
        } finally {
            USER_LOCK.unlock();
        }
    }

    private Stream<UserDto> searchStream(Predicate<UserDto> criteria) {

        return users.stream().filter(criteria);
    }
}
