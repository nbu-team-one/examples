package com.bunis.um.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.ResourceBundle;

@WebFilter(servletNames = {"index"}, filterName = "ce_filter")
public class CharacterEncodingFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String characterEncoding = request.getCharacterEncoding();
        String utf8 = StandardCharsets.UTF_8.name();
        if (!utf8.equalsIgnoreCase(characterEncoding)) {
            request.setCharacterEncoding(utf8);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
