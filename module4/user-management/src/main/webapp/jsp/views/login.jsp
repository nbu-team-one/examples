<%--
  Created by IntelliJ IDEA.
  User: nbu
  Date: 6/11/20
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form" %>
<%@ page import="com.bunis.um.command.CommandType" %>

<div class="container">
    <div class="content">
        <c:if test="${not empty error}">
            <p class="is-danger"><c:out value="${error}"/></p>
        </c:if>

        <form action="${pageContext.request.contextPath}/" method="post">
            <input type="hidden" name="_command" value="${CommandType.LOGIN_SUBMIT}">
            <div class="field">
                <div class="field-label">
                    <label class="label" for="user.loginName">
                        <fmt:message key="user.loginName"/>
                    </label>
                </div>
                <div class="field-body">
                    <div class="control">
                        <input id="user.loginName" name="user.loginName" class="input" type="text">
                    </div>
                </div>
            </div>
            <div class="field">
                <div class="field-label">
                    <label class="label" for="user.password">
                        <fmt:message key="user.password"/>
                    </label>
                </div>
                <div class="field-body">
                    <div class="control">
                        <input id="user.password" name="user.password" class="input" type="text">
                    </div>
                </div>
            </div>
            <div class="field">
                <p class="control">
                    <fmt:message var="login_label" key="links.user.login"/>
                    <input class="button is-primary" type="submit" value="${login_label}">
                </p>
            </div>
        </form>
    </div>
</div>
