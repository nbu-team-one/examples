<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div>
    <form action="${pageContext.request.contextPath}">
        <div class="field">
            <label class="label">
                <fmt:message key="user.name" />
            </label>
            <div class="control">
                <input class="input" name="user.name" type="text" placeholder="Text input">
            </div>
            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-link">
                            <fmt:message key="form.save" />
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>