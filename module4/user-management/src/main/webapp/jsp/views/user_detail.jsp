<%--
  Created by IntelliJ IDEA.
  User: nbu
  Date: 2019-02-11
  Time: 23:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form" %>
<%@ page import="com.bunis.um.command.CommandType" %>

<div class="container">
    <div class="columns">
        <div class="is-one-third">
            <div class="card">
                <div class="card-content">
                    <div class="media-content">
                        <p class="title is-4"><c:out value="${user.firstName}"/> <c:out value="${user.lastName}"/></p>
                        <p class="subtitle is-6">@<c:out value="${user.loginName}"/></p>
                    </div>
                    <div class="content">
                        <form:display fieldName="email" beanName="user"/>
                        <form:display fieldName="ipAddress" beanName="user"/>
                        <form:display fieldName="phoneNumber" beanName="user"/>
                    </div>
                    <form:form commandName="${CommandType.DELETE_USER}">
                        <fmt:message var="delete_label" key="links.user.delete"/>
                        <input type="hidden" name="user.id" value="${user.id}">
                        <input class="button is-danger" type="submit" value="${delete_label}">
                    </form:form>
                    <form:form commandName="${CommandType.EDIT_VIEW_USER}">
                        <fmt:message var="edit_label" key="links.user.edit"/>
                        <input type="hidden" name="user.id" value="${user.id}">
                        <input class="button is-danger" type="submit" value="${edit_label}">
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
