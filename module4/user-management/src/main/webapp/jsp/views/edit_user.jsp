<%--
  Created by IntelliJ IDEA.
  User: nbu
  Date: 2019-02-11
  Time: 23:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form" %>
<%@ page import="com.bunis.um.command.CommandType" %>

<div class="container">
    <div class="columns">
        <div class="is-one-third">
            <div class="card">
                <div class="card-content">
                    <div class="media-content">
                        <p class="title is-4"><c:out value="${user.firstName}"/> <c:out value="${user.lastName}"/></p>
                        <p class="subtitle is-6">@<c:out value="${user.loginName}"/></p>
                    </div>

                    <pre>
                        userId: <%= request.getParameter("user.id") %>

                        command: <%= request.getParameter("_command") %>
                    </pre>

                    <c:if test="${not empty errorMsg}">
                        <p class="error">
                            ${errorMsg}
                        </p>
                    </c:if>

                    <form action="${pageContext.request.contextPath}/" method="post">
                        <div class="content">
                            <p>
                                <label for="user.loginName">
                                    Edit user login name
                                </label>
                                <input id="user.loginName" type="text" name="user.loginName" value="${user.loginName}" />
                            </p>
                        </div>
                        <fmt:message var="save_label" key="links.user.save"/>
                        <input type="hidden" name="_command" value="${CommandType.EDIT_SAVE_USER}">
                        <input type="hidden" name="user.id" value="${user.id}">
                        <input class="button is-danger" type="submit" value="${save_label}">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

