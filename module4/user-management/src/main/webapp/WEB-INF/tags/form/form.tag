<%@ tag import="com.bunis.um.command.CommandType" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:directive.attribute name="commandName" type="com.bunis.um.command.CommandType" required="true"
                         description="Command name to submit"/>

<c:if test="${empty commandName}">
    <c:set var="commandName" value="${CommandType.INDEX}"/>
</c:if>
<div class="tag-form">
    <form action="${pageContext.request.contextPath}/" method="post">
        <input type="hidden" name="_command" value="${commandName}">
        <jsp:doBody/>
    </form>
</div>