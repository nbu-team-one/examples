package com.bunis.um.service;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.bunis.um.dto.UserDto;

@RunWith(JUnit4.class)
public class UserServiceTest {


    @Test
    public void shouldGetAll() {

        ResourceBundle rb = ResourceBundle.getBundle("i18n/ApplicationMessages", new Locale("ru"));

        String string = rb.getString("links.user.list");

        UserService userService = UserServiceFactory.getUserService();
        List<UserDto> userDtos = userService.find(dto -> true);
        Assert.assertEquals(20, userDtos.size());
    }
}
