package com.bunis.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

public class SocketApplication {

    private static final AtomicInteger COUNTER = new AtomicInteger(0);

    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket server = new ServerSocket(59090);


        boolean isRunning = true;
        while (isRunning) {

            Socket client = server.accept();
            OutputStream outputStream = client.getOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(outputStream);
            InputStream inputStream = client.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputStreamReader);

            writer.write("Hello?");
            writer.flush();
            String line = reader.readLine();
            writer.write("Echo: " + line);
            writer.flush();

            writer.append("MessageCount: ")
                    .append(String.valueOf(COUNTER.incrementAndGet()))
                    .append("\n")
                    .append("wait for a second...")
                    .append("\n")
                    .flush();
            client.close();
//            Thread.currentThread().sleep(1000);

//            if (COUNTER.get() % 3 == 0) {
//                writer.append("This is the end.").flush();
//                client.close();
//                isRunning = false;
//            }
        }
    }
}
