package com.bunis.upload;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

@MultipartConfig
@WebServlet(name = "upload", urlPatterns = "/*")
public class UploadServlet extends HttpServlet {

    public static final String GET_RESPONSE = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "\n" +
            "<head>\n" +
            "    <title>Upload</title>\n" +
            "</head>\n" +
            "\n" +
            "<body>\n" +
            "\t<h3>Do upload!</h3>\n" +
            "    <div class=\"main\">\n" +
            "        <form action=\"\" method=\"POST\" enctype=\"multipart/form-data\">\n" +
            "            <label>Please upload file:\n" +
            "                <input type=\"file\" name=\"uploadFile\" value=\"\">\n" +
            "            </label>\n" +
            "            <label>\n" +
            "                Please enter comment:\n" +
            "                <input type=\"text\" name=\"comment\" value=\"\">\n" +
            "            </label>\n" +
            "            <button type=\"submit\">Send data</button>\n" +
            "        </form>\n" +
            "    </div>\n" +
            "</body>\n" +
            "\n" +
            "</html>";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        resp.setContentLength(GET_RESPONSE.getBytes().length);
        resp.getWriter().write(GET_RESPONSE);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Part filePart = req.getPart("uploadFile");
        String comment = req.getParameter("comment");

        resp.sendRedirect(req.getContextPath());
    }
}
