package com.bunis.command;

public class AppCommandException extends Exception {

    private final String messageKey;

    public AppCommandException(String message, String messageKey) {
        super(message);
        this.messageKey = messageKey;
    }

    public AppCommandException(String message, Throwable cause, String messageKey) {
        super(message, cause);
        this.messageKey = messageKey;
    }
}
