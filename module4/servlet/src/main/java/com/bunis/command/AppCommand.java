package com.bunis.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface AppCommand {

    void execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException;
}
