package com.bunis.command;

import com.bunis.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserCommand implements AppCommand {

    private UserService userService;

    public UserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException {

        try {
            resp.getWriter().println(userService.getMessage());
        } catch (IOException e) {
            throw new AppCommandException("failed to write message", e, messageKey);
        }
    }
}
