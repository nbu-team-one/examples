package com.bunis.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/users", loadOnStartup = 2)
public class UserServlet extends HttpServlet {

    private static final long serialVersionUID = -2264698299307008141L;
    private static final String ID_FOR_DELETE_PARAM = "idForDelete";
    private static final String METHOD_PARAM = "_method";
    private static final String DELETE_METHOD = "DELETE";
    private static final String USER_NAME_PARAM = "userName";
    private final AtomicInteger counter = new AtomicInteger(0);
    private Map<Integer, String> names;

    @Override
    public void init(ServletConfig config) throws ServletException {
        names = new ConcurrentHashMap<>();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        StringBuilder builder = new StringBuilder();
        builder.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "\n" +
                "<head>\n" +
                "    <title>User management</title>\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "    <h1>Hello App</h1>\n" +
                "    <form action=\"users\" method=\"POST\">\n" +
                "        <div>\n" +
                "            <p>\n" +
                "                <label for=\"" + USER_NAME_PARAM + "\">\n" +
                "                    Name:\n" +
                "                </label>\n" +
                "                <input id=\"" + USER_NAME_PARAM + "\" type=\"text\" name=\"" + USER_NAME_PARAM + "\">\n" +
                "            </p>\n" +
                "            <p>\n" +
                "                <input type=\"submit\" value=\"Save\">\n" +
                "            </p>\n" +
                "        </div>\n" +
                "    </form>\n" +
                "    <div>\n" +
                "        <ul>");


        builder.append(names.entrySet().stream().map(entry -> "<li>\n" +
                "                <form action=\"users\" method=\"POST\">\n" +
                "                    <label>" + entry.getValue() + "</label>\n" +
                "                    <input type=\"hidden\" name=\"" + METHOD_PARAM + "\" value=\"" + DELETE_METHOD + "\">\n" +
                "                    <input type=\"submit\" value=\"Delete\">\n" +
                "                    <input type=\"hidden\" name=\"" + ID_FOR_DELETE_PARAM + "\" value=\"" + entry.getKey() + "\">\n" +
                "                </form>\n" +
                "            </li>").collect(Collectors.joining("\n")));

                builder.append("</ul>\n" +
                "    </div>\n" +
                "</body>\n" +
                "\n" +
                "</html>");

        writer.println(builder.toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String method = req.getParameter(METHOD_PARAM);
        if (DELETE_METHOD.equals(method)) {
            doDelete(req, resp);
        } else {
            String userName = req.getParameter(USER_NAME_PARAM);
            if (userName != null && !userName.isEmpty()) {
                names.put(counter.incrementAndGet(), userName);
            }

            doGet(req, resp);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Integer idForDelete = Integer.valueOf(req.getParameter(ID_FOR_DELETE_PARAM));
        this.names.remove(idForDelete);

        doGet(req, resp);
    }
}
