package com.bunis.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@WebServlet(urlPatterns = "/users/jsp")
public class JSPUserServlet extends HttpServlet {

    private static final long serialVersionUID = 3890879827874230836L;
    private static final String METHOD_PARAM = "_method";
    private static final String DELETE_METHOD = "DELETE";
    private static final String USER_NAME_PARAM = "userName";
    private static final String ID_FOR_DELETE_PARAM = "idForDelete";
    private final AtomicInteger counter = new AtomicInteger(0);
    private List<UserRecord> userList = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("userList", userList);
        req.getRequestDispatcher("/jsp/user/user-list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String method = req.getParameter(METHOD_PARAM);
        if (DELETE_METHOD.equals(method)) {
            doDelete(req, resp);
        } else {
            String userName = req.getParameter(USER_NAME_PARAM);
            if (userName != null && !userName.isEmpty()) {
                UserRecord userRecord = new UserRecord();
                userRecord.setId(counter.incrementAndGet());
                userRecord.setName(userName);
                userList.add(userRecord);
            } else {
                req.setAttribute("errors", Collections.singleton("User name is empty"));
            }
            doGet(req, resp);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            Integer idForDelete = Integer.valueOf(req.getParameter(ID_FOR_DELETE_PARAM));
            userList.removeIf(u -> u.getId().equals(idForDelete));
        } catch (Exception e) {
            req.setAttribute("errors", Collections.singleton("Failed to delete user with id" + req.getParameter(ID_FOR_DELETE_PARAM)));
        }

        doGet(req, resp);
    }

    public class UserRecord {

        private String name;
        private Integer id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
