package com.bunis.servlet;

import com.bunis.ApplicationContext;
import com.bunis.command.AppCommandException;
import com.bunis.command.AppCommandProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/userCommand")
public class UserCommandServlet extends HttpServlet {

    private AppCommandProvider commandProvider;

    @Override
    public void init() throws ServletException {

        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String commandName = req.getParameter("commandName");
        try {
            commandProvider.get(commandName).execute(req, resp);
        } catch (AppCommandException e) {
            // LOGGER
            // fallback
        } catch (Exception e) {

        }

    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
