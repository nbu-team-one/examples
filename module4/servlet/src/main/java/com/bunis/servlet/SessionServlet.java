package com.bunis.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

@WebServlet(urlPatterns = "/session")
public class SessionServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        if (session != null) {
            Enumeration<String> attributeNames = session.getAttributeNames();
            List<SessionAttribute> sessionAttributes = new ArrayList<>();
            while (attributeNames.hasMoreElements()) {
                String name = attributeNames.nextElement();
                Object sessionAttribute = session.getAttribute(name);
                SessionAttribute attribute = new SessionAttribute();
                attribute.setName(name);
                attribute.setValue(String.valueOf(sessionAttribute));
                sessionAttributes.add(attribute);
            }

            req.setAttribute("sessionAttributes", sessionAttributes);
        } else {
            req.setAttribute("do_login", true);
        }

        req.getRequestDispatcher("/jsp/session.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        String method = req.getParameter("_method");
        String attrName = req.getParameter("attrName");
        String attrValue = req.getParameter("attrValue");
        if ("DELETE".equals(method) && attrName != null) {
            session.removeAttribute(attrName);
        } else if ("ADD".equals(method)) {
            session.setAttribute(attrName, attrValue);
        } else if ("LOGIN".equals(method)) {
            req.getSession(true).setAttribute("LOGON_ID", UUID.randomUUID().toString());
            req.setAttribute("do_login", false);
        } else if ("LOGOUT".equals(method)) {
            req.getSession(false).invalidate();
        }

        resp.sendRedirect("session");
    }

    public class SessionAttribute {
        private String name;
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
