package com.bunis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@WebServlet(urlPatterns = "/app", loadOnStartup = 1, name = "AppServlet")
public class AppServlet extends HttpServlet {

    private static final long serialVersionUID = -5564701017960799554L;
    private final static Logger LOGGER = Logger.getLogger(AppServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Set response content type
        response.setContentType("text/html");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println("<h1>" + getServletContext().getAttribute("configMessage") + "</h1>");

        LOGGER.info("Hello");
    }
}
