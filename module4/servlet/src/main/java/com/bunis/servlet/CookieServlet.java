package com.bunis.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet(urlPatterns = "/cookie")
public class CookieServlet extends HttpServlet {

    private final static String COOKIES_LAYOUT = "Cookie: Name - {0}, Value - {1}, Age - {2}";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            Stream<Cookie> cookieStream = Stream.of(cookies);
            List<String> cookiesList = cookieStream.map(c ->
                    MessageFormat.format(COOKIES_LAYOUT, c.getName(), c.getValue(), c.getMaxAge()))
                    .collect(Collectors.toList());

            req.setAttribute("cookiesList", cookiesList);
        }
        req.getRequestDispatcher("/jsp/cookie.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String cookieName = req.getParameter("cookieName");
        String cookieValue = req.getParameter("cookieValue");
        if (cookieName != null && cookieValue != null) {
            Cookie cookie = new Cookie(cookieName, cookieValue);
            cookie.setMaxAge(60 * 60 * 2);
            resp.addCookie(cookie);
        }

        resp.sendRedirect("cookie");
    }
}
