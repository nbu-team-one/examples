package com.bunis.filter;

import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import java.awt.List;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class CommonRequestFilter implements Filter {

    private final static Logger LOGGER = Logger.getLogger(CommonRequestFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        LOGGER.info("Filter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        LOGGER.info("Process request");
        LOGGER.info("Request details: " + request.getLocalName() + " " + request.getLocalAddr());
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

        LOGGER.info("Filter destroyed");
    }
}
