package com.bunis.listener;

import org.apache.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

@WebListener
public class SessionAttributeListener implements HttpSessionAttributeListener {

    private final static Logger LOGGER = Logger.getLogger(SessionAttributeListener.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {

        LOGGER.info("added attribute " + event.getName() + ":" + event.getValue() + " into session " + event.getSession().getId());

    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {

        LOGGER.info("attribute removed " + event.getName() + ":" + event.getValue() + " into session " + event.getSession().getId());
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

        LOGGER.info("attribute replaced " + event.getName() + ":" + event.getValue() + " into session " + event.getSession().getId());
    }
}
