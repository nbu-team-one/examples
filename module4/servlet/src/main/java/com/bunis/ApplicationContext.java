package com.bunis;


import com.bunis.command.AppCommand;
import com.bunis.command.AppCommandProvider;
import com.bunis.command.UserCommand;
import com.bunis.service.UserService;
import com.bunis.service.UserServiceImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Should be a Singleton
 */
public class ApplicationContext {

    private static final ApplicationContext INSTANCE = new ApplicationContext();


    private final Map<Class<?>, Object> beans = new HashMap<>();


    public static ApplicationContext getInstance() {

        return INSTANCE;
    }

    public void initialize() {

        // create DAO
        UserService userService = new UserServiceImpl();
        AppCommand userCommand = new UserCommand(userService);
        AppCommandProvider commandProvider = new AppCommandProvider() {

            private final Map<String, AppCommand> commandMap = new HashMap<>();

            @Override
            public void register(String commandName, AppCommand command) {

                this.commandMap.put(commandName, command);
            }

            @Override
            public void remove(String commandName) {

                this.commandMap.remove(commandName);
            }

            @Override
            public AppCommand get(String commandName) {

                return this.commandMap.get(commandName);
            }
        };
        commandProvider.register("user", userCommand);

        beans.put(userService.getClass(), userService);
        beans.put(commandProvider.getClass(), commandProvider);
    }

    public void destroy() {

        beans.clear();
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }
}
