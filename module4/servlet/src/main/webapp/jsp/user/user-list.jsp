<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <title>User list (advanced)</title>
</head>

<body>
    <h1>Get user list and manage it with JSP</h1>
    <c:if test="${not empty errors}">
        <c:forEach items="${errors}" var="error">
            <p style="color: red">${error}</p>
        </c:forEach>
    </c:if>
    <form action="jsp" method="POST">
        <div>
            <p>
                <label for="userName">
                    Name:
                </label>
                <input id="userName" type="text" name="userName">
            </p>
            <p>
                <input type="submit" value="Save">
            </p>
        </div>
    </form>
    <div>
        <c:if test="${not empty userList}">
            <ul>
                <c:forEach items="${userList}" var="user">
                    <li>
                        <form action="jsp" method="POST">
                            <label>${user.name}</label>
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Delete">
                            <input type="hidden" name="idForDelete" value="${user.id}">
                        </form>
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </div>
</body>

</html>