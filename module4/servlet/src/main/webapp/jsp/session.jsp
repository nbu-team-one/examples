<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <title>Work with session</title>
</head>

<body>
    <h1>Display session attributes</h1>
    <c:if test="${not empty errors}">
        <c:forEach items="${errors}" var="error">
            <p style="color: red">${error}</p>
        </c:forEach>
    </c:if>
        <form action="session" method="POST">
            <div>
            <input type="hidden" name="_method" value="ADD"/>
                <p>
                    <label for="attrName">
                        Name:
                    </label>
                    <input id="attrName" type="text" name="attrName">
                </p>
                <p>
                    <label for="attrValue">
                        Value:
                    </label>
                    <input id="attrValue" type="text" name="attrValue">
                </p>
                <p>
                    <input type="submit" value="Add">
                </p>
            </div>
        </form>
    <div>
        <c:if test="${not empty sessionAttributes}">
            <ul>
                <c:forEach items="${sessionAttributes}" var="attr">
                    <li>
                        <form action="session" method="POST">
                                <input type="hidden" name="_method" value="DELETE"/>
                                <input type="hidden" name="attrName" value="${attr.name}"/>
                                <span>Name: ${attr.name}<span>
                                <span>Value: ${attr.value}<span>
                                <p>
                                    <input type="submit" value="Delete">
                                </p>
                        </form>
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </div>
    <c:choose>
        <c:when test="${do_login}">
            <div>
                <h3>Login</h3>
                 <form action="session" method="POST">
                 <input type="hidden" name="_method" value="LOGIN"/>
                 <p>
                     <input type="submit" value="Log in">
                 </p>
                 </form>
            </div>
        </c:when>
        <c:otherwise>
            <div>
                <h3>Log out</h3>
                 <form action="session" method="POST">
                 <input type="hidden" name="_method" value="LOGOUT"/>
                 <p>
                     <input type="submit" value="Log out">
                 </p>
                 </form>
            </div>
        </c:otherwise>
    </c:choose>
</body>

</html>