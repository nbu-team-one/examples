<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <title>Work with cookie</title>
</head>

<body>
    <h1>Display client cookies</h1>
    <c:if test="${not empty errors}">
        <c:forEach items="${errors}" var="error">
            <p style="color: red">${error}</p>
        </c:forEach>
    </c:if>
        <form action="cookie" method="POST">
            <div>
                <p>
                    <label for="cookieName">
                        Name:
                    </label>
                    <input id="cookieName" type="text" name="cookieName">
                </p>
                <p>
                    <label for="cookieValue">
                        Value:
                    </label>
                    <input id="cookieValue" type="text" name="cookieValue">
                </p>
                <p>
                    <input type="submit" value="Save">
                </p>
            </div>
        </form>
    <div>
        <c:if test="${not empty cookiesList}">
            <ul>
                <c:forEach items="${cookiesList}" var="cookieItem">
                    <li>
                        ${cookieItem}
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </div>
</body>

</html>