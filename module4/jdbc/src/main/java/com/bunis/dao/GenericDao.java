package com.bunis.dao;

import com.bunis.jdbc.ConnectionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class GenericDao<T extends IdentifiedRow> implements CRUDDao<T> {

    private static final String SELECT_ALL_QUERY = "select * from {0}";
    private static final String SELECT_BY_ID_QUERY = "select * from {0} where id = ?";
    private static final String INSERT_QUERY = "insert into {0} ({1}) values ({2})";
    private static final String UPDATE_QUERY = "update {0} set {1} where id = ?";
    private static final String DELETE_QUERY = "delete from {0} where id = ?";
    private static final String SELECT_MAX_ID_QUERY = "select max(id) from {0}";


    private final String tableName;
    private final IdentifiedRowMapper<T> rowMapper;

    public GenericDao(String tableName, IdentifiedRowMapper<T> rowMapper) {
        this.tableName = tableName;
        this.rowMapper = rowMapper;
    }


    @Override
    public Long save(T entity) throws SQLException {
        AtomicLong id = new AtomicLong(-1L);
        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            List<String> columnNames = rowMapper.getColumnNames();
            String columns = String.join(", ", columnNames);
            String wildcards = columnNames.stream()
                    .map(column -> "?")
                    .collect(Collectors.joining(", "));

            String sql = MessageFormat.format(INSERT_QUERY, tableName, columns, wildcards);
            try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                rowMapper.populateStatement(statement, entity);
                statement.executeUpdate();
                ResultSet generatedKeys = statement.getGeneratedKeys();
                while (generatedKeys.next()) {
                    id.set(generatedKeys.getLong(1));
                }
            }
        });
        return id.get();
    }

    @Override
    public void update(T entity) throws SQLException {
        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            List<String> columnNames = rowMapper.getColumnNames();
            String columns = columnNames.stream()
                    .map(column -> column + " = ?")
                    .collect(Collectors.joining(", "));

            String sql = MessageFormat.format(UPDATE_QUERY, tableName, columns);
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                rowMapper.populateStatement(statement, entity);
                statement.setLong(columnNames.size() + 1, entity.getId());
                statement.executeUpdate();
            }
        });
    }

    @Override
    public void delete(T entity) throws SQLException {

        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            String sql = MessageFormat.format(DELETE_QUERY, tableName);
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setLong(1, entity.getId());
                statement.executeUpdate();
            }
        });
    }

    @Override
    public T getById(Long id) throws SQLException {

        AtomicReference<T> result = new AtomicReference<>();
        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            String sql = MessageFormat.format(SELECT_BY_ID_QUERY, tableName);
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setLong(1, id);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    result.set(rowMapper.map(resultSet));
                }
            }
        });
        return result.get();
    }

    @Override
    public List<T> findAll() throws SQLException {
        List<T> result = new ArrayList<>();
        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            String sql = MessageFormat.format(SELECT_ALL_QUERY, tableName);
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    result.add(rowMapper.map(resultSet));
                }
            }
        });
        return result;
    }
}
