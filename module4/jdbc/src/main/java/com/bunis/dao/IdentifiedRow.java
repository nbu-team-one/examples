package com.bunis.dao;

public interface IdentifiedRow {

    Long getId();

    void setId(Long id);
}
