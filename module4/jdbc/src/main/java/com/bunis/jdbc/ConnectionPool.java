package com.bunis.jdbc;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection();
}
