package com.bunis.jdbc;

import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BasicConnectionPool implements ConnectionPool {

    private final static Logger LOGGER = Logger.getLogger(BasicConnectionPool.class);

    private final String driverClass;
    private final String jdbcUrl;
    private final String user;
    private final String password;
    private final int poolCapacity;

    private final Lock connectionLock = new ReentrantLock();
    private final Condition emptyPool = connectionLock.newCondition();

    private final LinkedList<Connection> availableConnections = new LinkedList<>();
    private final LinkedList<Connection> usedConnections = new LinkedList<>();

    public BasicConnectionPool(String driverClass, String jdbcUrl, String user, String password, int poolCapacity) {

        this.driverClass = driverClass;
        this.jdbcUrl = jdbcUrl;
        this.user = user;
        this.password = password;
        this.poolCapacity = poolCapacity;

        initDriver(this.driverClass);
    }

    @Override
    public Connection getConnection() {

        connectionLock.lock();
        Connection proxyConnection = null;
        try {

            if (availableConnections.isEmpty() && usedConnections.size() == poolCapacity) {
                try {
                    emptyPool.await();
                } catch (InterruptedException e) {
                    LOGGER.error(e);
                    throw new IllegalThreadStateException(e.getMessage());
                }
            }

            if (availableConnections.isEmpty() && usedConnections.size() < poolCapacity) {
                Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
                availableConnections.add(connection);
            } else if (availableConnections.isEmpty()) {
                throw new IllegalStateException("Get Maximum pool size was reached");
            }
            Connection connection = availableConnections.removeFirst();
            usedConnections.add(connection);
            proxyConnection = createProxyConnection(connection);
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            connectionLock.unlock();
        }
        return proxyConnection;
    }

    private void initDriver(String driverClass) {
        try {
            Class.forName(driverClass);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e);
            throw new IllegalStateException("Driver cannot be found", e);
        }
    }

    private Connection createProxyConnection(Connection connection) {

        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else if ("hashCode".equals(method.getName())) {
                        return connection.hashCode();
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    void releaseConnection(Connection connection) {
        try {
            connectionLock.lock();
            if (availableConnections.size() >= poolCapacity) {
                throw new IllegalStateException("Release Maximum pool size was reached");
            }

            if (!connection.isClosed()) {
                connection.close();
            }
            usedConnections.remove(connection);
            availableConnections.add(connection);
            emptyPool.signal();
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            connectionLock.unlock();
        }
    }
}
