package com.bunis.jdbc;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionManager {

    private static final String JDBCDRIVER_CLASS = "org.hsqldb.jdbc.JDBCDriver";
    private static final String DB_URL = "jdbc:hsqldb:mem:testdb;DB_CLOSE_DELAY=-1";
    private static final String DB_USER = "sa";
    private static final Lock LOCK = new ReentrantLock();
    private static final Logger LOGGER = Logger.getLogger(ConnectionManager.class);
    private static volatile ConnectionManager INSTANCE;

    private final Lock connectionLock = new ReentrantLock();

    private ConnectionManager() {

    }

    public static ConnectionManager getInstance() {

        if (INSTANCE == null) {
            LOCK.lock();
            try {
                if (INSTANCE == null) {
                    INSTANCE = new ConnectionManager();
                    INSTANCE.initDriver();
                }
            } catch (ClassNotFoundException e) {
                LOGGER.error(e);
                throw new IllegalStateException(e);
            } finally {
                LOCK.unlock();
            }
        }
        return INSTANCE;
    }

    public void doWithConnection(ConnectionConsumer consumer) throws SQLException {
        connectionLock.lock();
        try (Connection connection = getConnection()) {
            LOGGER.info("Before accept");
            consumer.accept(connection);
            LOGGER.info("After accept");
        } finally {
            connectionLock.unlock();
        }
    }

    public void doWithConnectionQuite(ConnectionConsumer consumer) throws SQLException {
        connectionLock.lock();
        try (Connection connection = getConnection()) {
            consumer.accept(connection);
        } finally {
            connectionLock.unlock();
        }
    }

    private void initDriver() throws ClassNotFoundException {
        Class.forName(JDBCDRIVER_CLASS);
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, DB_USER, null);
    }

    public interface ConnectionConsumer {

        void accept(Connection connection) throws SQLException;
    }
}
