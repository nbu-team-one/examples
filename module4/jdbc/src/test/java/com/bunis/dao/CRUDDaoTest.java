package com.bunis.dao;

import com.bunis.entity.User;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static com.bunis.TestUtil.createUserTable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class CRUDDaoTest {

    private static final Logger LOGGER = Logger.getLogger(CRUDDaoTest.class);
    private static final String TABLE_NAME = "users";

    @BeforeClass
    public static void initDataBase() throws SQLException {

        createUserTable();
    }

    @Test
    public void shouldDoAllOperations() throws SQLException {

        CRUDDao<User> userDao = new GenericDao<>(TABLE_NAME, getUserRowMapper());
        User user = new User();
        user.setEmail("tpallant0@uol.com.br");
        user.setName("Teodoro");
        Long newId = userDao.save(user);
        List<User> all = userDao.findAll();
        assertEquals(1, all.size());
        all.forEach(LOGGER::info);

        User dbUser = userDao.getById(newId);
        dbUser.setName("Anetta");
        userDao.update(dbUser);
        User byId = userDao.getById(dbUser.getId());
        assertNotNull(byId);
        assertEquals("Anetta", byId.getName());

        userDao.delete(byId);
        List<User> empty = userDao.findAll();
        assertEquals(0, empty.size());
    }

    private IdentifiedRowMapper<User> getUserRowMapper() {
        return new IdentifiedRowMapper<User>() {
            @Override
            public User map(ResultSet resultSet) throws SQLException {
                User user = new User();
                user.setName(resultSet.getString("name"));
                user.setEmail(resultSet.getString("email"));
                user.setId(resultSet.getLong("id"));
                return user;
            }

            @Override
            public List<String> getColumnNames() {
                return Arrays.asList("name", "email");
            }

            @Override
            public void populateStatement(PreparedStatement statement, User entity) throws SQLException {

                statement.setString(1, entity.getName());
                statement.setString(2, entity.getEmail());
            }
        };
    }
}
