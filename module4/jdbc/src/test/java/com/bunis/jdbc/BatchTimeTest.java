package com.bunis.jdbc;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.IntStream;

import static com.bunis.TestUtil.createUserTable;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class BatchTimeTest {

    private static final Logger LOGGER = Logger.getLogger(BatchTimeTest.class);
    private static final int ITEMS_COUNT = 1_000_000;

    @BeforeClass
    public static void initDriverAndDataBase() throws SQLException {

        createUserTable();
    }

    @Test
    public void shouldInsertAllInSeparateConnection() throws SQLException {
        long start = System.currentTimeMillis();

        String sql = "INSERT INTO users (name, email) VALUES (?, ?)";
        IntStream.range(0, ITEMS_COUNT).forEach(i -> {
            try {
                ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
//                    connection.setAutoCommit(false);
                    try (PreparedStatement statement = connection.prepareStatement(sql)) {
                        statement.setString(1, "test_1_name" + i);
                        statement.setString(2, "test_1_email" + i);
                        int inserted = statement.executeUpdate();
                        assertEquals(1, inserted);
//                        connection.commit();
                    } catch (SQLException e) {
                        try {
                            connection.rollback();
                        } catch (SQLException e1) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });

        long end = System.currentTimeMillis();
        LOGGER.info("separate connection execution time = " + (end - start));

        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            try (PreparedStatement statement =
                         connection.prepareStatement("select count(id) from users where name like 'test_1_name%'")) {
                try (ResultSet resultSet = statement.executeQuery()) {
                    resultSet.next();
                    int count = resultSet.getInt(1);
                    assertEquals(ITEMS_COUNT, count);
                }
            }
        });
    }

    @Test
    public void shouldInsertAllInOneConnection() throws SQLException {
        long start = System.currentTimeMillis();

        String sql = "INSERT INTO users (name, email) VALUES (?, ?)";
        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            connection.setAutoCommit(false);
            IntStream.range(0, ITEMS_COUNT).forEach(i -> {
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setString(1, "test_2_name" + i);
                    statement.setString(2, "test_2_email" + i);
                    int inserted = statement.executeUpdate();
                    assertEquals(1, inserted);
                    connection.commit();
                } catch (SQLException e) {
                    try {
                        connection.rollback();
                    } catch (SQLException e1) {
                        throw new RuntimeException(e);
                    }
                }
            });
        });


        long end = System.currentTimeMillis();
        LOGGER.info("one connection execution time = " + (end - start));

        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            try (PreparedStatement statement =
                         connection.prepareStatement("select count(id) from users where name like 'test_2_name%'")) {
                try (ResultSet resultSet = statement.executeQuery()) {
                    resultSet.next();
                    int count = resultSet.getInt(1);
                    assertEquals(ITEMS_COUNT, count);
                }
            }
        });
    }

    @Test
    public void shouldInsertAllInBatch() throws SQLException {
        long start = System.currentTimeMillis();
        String sql = "INSERT INTO users (name, email) VALUES (?, ?)";
        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                IntStream.range(0, ITEMS_COUNT).forEach(i -> {
                    try {
                        statement.setString(1, "test_batch_name" + i);
                        statement.setString(2, "test_batch_email" + i);
                        statement.addBatch();
                        if (i % 1_000 == 0) {
                            statement.executeBatch();
                        } else if (i + 1 == ITEMS_COUNT) {
                            statement.executeBatch();
                            connection.commit();
                        }
                    } catch (SQLException e) {
                        try {
                            connection.rollback();
                        } catch (SQLException e1) {
                            throw new RuntimeException(e);
                        }
                        throw new RuntimeException(e);
                    }
                });
            }
        });
        long end = System.currentTimeMillis();
        LOGGER.info("batch execution time = " + (end - start));
        ConnectionManager.getInstance().doWithConnectionQuite(connection -> {
            try (PreparedStatement statement =
                         connection.prepareStatement("select count(id) from users where name like 'test_batch_name%'")) {
                try (ResultSet resultSet = statement.executeQuery()) {
                    resultSet.next();
                    int count = resultSet.getInt(1);
                    assertEquals(ITEMS_COUNT, count);
                }
            }
        });
    }
}
