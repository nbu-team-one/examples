package com.bunis.jdbc;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static com.bunis.TestUtil.createUserTable;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ConnectionManagerTest {

    private static final Logger LOGGER = Logger.getLogger(ConnectionManagerTest.class);

    @BeforeClass
    public static void initDriverAndDataBase() throws SQLException {

        createUserTable();
    }

    @Test
    public void shouldSupportConcurrentAccess() throws SQLException, InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(5);

        IntStream.range(0, 5).forEach(i ->
                executorService.submit(() -> {
                    sleep();
                    try {
                        LOGGER.info("Trying get connection");
                        ConnectionManager.getInstance().doWithConnection(connection -> {
                            sleep();
                            insertData(connection, "name" + i, "email" + i);
                            sleep();
                        });
                    } catch (SQLException e) {
                        LOGGER.error(e);
                    }
                }));
        executorService.awaitTermination(5, TimeUnit.SECONDS);
        Set<Integer> ids = new HashSet<>();

        ConnectionManager.getInstance().doWithConnection(connection -> {
            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM users")) {
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        String id = resultSet.getString("id");
                        ids.add(Integer.valueOf(id));
                    }
                }
            }
        });
        assertEquals(5, ids.size());
    }

    private void sleep() {
        try {
            Thread.sleep(Math.abs(new Random().nextInt(2_000)));
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }

    private void insertData(Connection connection, String userName, String userEmail) throws SQLException {
        String insertSql = "INSERT INTO users (name, email) VALUES (?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(insertSql)) {
            statement.setString(1, userName);
            statement.setString(2, userEmail);
            int inserted = statement.executeUpdate();
            assertEquals(1, inserted);
        }
    }
}
