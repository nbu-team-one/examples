package com.bunis.jdbc;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.bunis.TestUtil.createUserTable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


@RunWith(JUnit4.class)
public class JDBCTest {

    @BeforeClass
    public static void initDriverAndDataBase() throws SQLException {

        createUserTable();
    }

    @Test
    public void shouldCreateTableInsertAndRead() throws SQLException {

        ConnectionManager.getInstance().doWithConnection(connection -> {
            String userName = "Jermaine";
            String userEmail = "jdonnellan0@un.org";
            String insertSql = "INSERT INTO users (name, email) VALUES ('" + userName + "', '" + userEmail + "')";
            PreparedStatement insertStatement = connection.prepareStatement(insertSql);
            int inserted = insertStatement.executeUpdate();
            insertStatement.close();
            assertEquals(1, inserted);

            String selectSql = "SELECT * FROM users WHERE name = 'alex'";
            PreparedStatement selectStatement = connection.prepareStatement(selectSql);
            ResultSet resultSet = selectStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                System.out.println("id = " + id);
                String name = resultSet.getString("name");
                assertEquals(userName, name);
                String email = resultSet.getString("email");
                assertEquals(userEmail, email);
            }

            resultSet.close();
            selectStatement.close();

        });
    }

    @Test
    public void shouldInsertRowsUsingWildcards() throws SQLException {

        ConnectionManager.getInstance().doWithConnection(connection -> {
            String insertSql = "INSERT INTO users (name, email) VALUES (?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(insertSql);
            String userName = "Susette";
            insertStatement.setString(1, userName);
            String userEmail = "smoulds1@studiopress.com";
            insertStatement.setString(2, userEmail);
            int inserted = insertStatement.executeUpdate();
            assertEquals(1, inserted);
            insertStatement.close();

            String selectSql = "SELECT * FROM users WHERE name = ?";
            PreparedStatement selectStatement = connection.prepareStatement(selectSql);
            selectStatement.setString(1, userName);
            ResultSet resultSet = selectStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                System.out.println("id = " + id);
                String name = resultSet.getString("name");
                assertEquals(userName, name);
                String email = resultSet.getString("email");
                assertEquals(userEmail, email);
            }

            resultSet.close();
            selectStatement.close();
        });
    }

    @Test
    public void shouldRollback() throws SQLException {

        ConnectionManager.getInstance().doWithConnection(connection -> {
            connection.setAutoCommit(false);

            String insertSql = "INSERT INTO users (name, email) VALUES (?, ?)";
            String userName = "sam";
            try (PreparedStatement insertStatement = connection.prepareStatement(insertSql)) {
                insertStatement.setString(1, userName);
                String userEmail = "seriose@gmail.com";
                insertStatement.setString(2, userEmail);
                int inserted = insertStatement.executeUpdate();
                assertEquals(1, inserted);
            } finally {
                connection.rollback();
            }

            String selectSql = "SELECT * FROM users WHERE name = ?";
            try (PreparedStatement selectStatement = connection.prepareStatement(selectSql)) {
                selectStatement.setString(1, userName);
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    boolean next = resultSet.next();
                    assertFalse(next);
                }
            }
        });
    }

    @Test
    public void shouldCommit() throws SQLException {

        String userName = "Tansy";
        String userEmail = "tkesten2@usgs.gov";

        ConnectionManager.getInstance().doWithConnection(connection -> {
            connection.setAutoCommit(false);

            String insertSql = "INSERT INTO users (name, email) VALUES (?, ?)";
            try (PreparedStatement insertStatement = connection.prepareStatement(insertSql)) {
                insertStatement.setString(1, userName);
                insertStatement.setString(2, userEmail);
                int inserted = insertStatement.executeUpdate();
                assertEquals(1, inserted);
                connection.commit();
            } catch (SQLException sqle) {
                connection.rollback();
            }
        });

        ConnectionManager.getInstance().doWithConnection(connection -> {
            String selectSql = "SELECT * FROM users WHERE name = ?";
            try (PreparedStatement selectStatement = connection.prepareStatement(selectSql)) {
                selectStatement.setString(1, userName);
                try (ResultSet resultSet = selectStatement.executeQuery()) {
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        System.out.println("id = " + id);
                        String name = resultSet.getString("name");
                        assertEquals(userName, name);
                        String email = resultSet.getString("email");
                        assertEquals(userEmail, email);
                    }
                }
            }
        });
    }
}
