package com.bunis.ajax;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

@WebServlet(urlPatterns = "/api")
public class AjaxServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(AjaxServlet.class);
    private static final long serialVersionUID = 5887112717519067184L;

    @Override
    public void init() throws ServletException {
        //
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("GET {}", req.getQueryString());
        writeResponse(resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
        LOGGER.info("POST {}", data);
        writeResponse(resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
        LOGGER.info("PUT {}", data);
        writeResponse(resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
        LOGGER.info("DELETE {}", data);
        writeResponse(resp);
    }

    private void writeResponse(HttpServletResponse resp) throws IOException {

        String response = "{\n" +
                "\t\"message\": \"success\"\n" +
                "}";
        resp.setContentType("application/json");
        resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
        resp.setContentLength(response.getBytes().length);
        resp.setStatus(200);
        try (PrintWriter writer = resp.getWriter()) {
            writer.write(response);
            writer.flush();
        }
    }
}
