package com.bunis.csv.application;


import com.bunis.csv.dao.PersonDao;
import com.bunis.csv.dao.PersonDaoImpl;
import com.bunis.csv.entity.Person;
import com.bunis.csv.service.PersonService;
import com.bunis.csv.service.PersonServiceImpl;
import com.bunis.csv.transaction.TransactionManager;
import com.bunis.csv.transaction.TransactionManagerImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.lang.reflect.InvocationHandler;

@RunWith(JUnit4.class)
public class TransactionManagerTest {

    @Test
    public void shouldBeginAndCommitTransaction() {

        TransactionManager trSpy = Mockito.spy(new TransactionManagerImpl());
        PersonDao dao = new PersonDaoImpl();
        PersonService personService = new PersonServiceImpl(dao);

        PersonService personServiceProxy = createProxy(trSpy, personService, PersonService.class);

        personServiceProxy.save(new Person());


        Mockito.verify(trSpy, Mockito.times(1)).begin();
        Mockito.verify(trSpy, Mockito.times(1)).commit();
    }

    private <T> T createProxy(TransactionManager trSpy, T personService, Class<T>... interfaces) {
        InvocationHandler transactionalInvocationHandler = AppServlet.createTransactionalInvocationHandler(trSpy, personService);
        return AppServlet.createProxy(this.getClass().getClassLoader(), transactionalInvocationHandler, interfaces);
    }
}

