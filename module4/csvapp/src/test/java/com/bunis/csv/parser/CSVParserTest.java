package com.bunis.csv.parser;

import com.bunis.csv.builder.EntityBuilder;
import com.bunis.csv.builder.EntityValidator;
import com.bunis.csv.builder.ValidationMessage;
import com.bunis.csv.builder.ValidationResult;
import com.bunis.csv.dao.PersonDao;
import com.bunis.csv.dao.PersonDaoImpl;
import com.bunis.csv.entity.Person;
import com.bunis.csv.person.PersonBuilder;
import com.bunis.csv.service.PersonServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class CSVParserTest {

    private PersonDao personDao;
    private PersonServiceImpl personService;

    @Before
    public void setUp() {

        personDao = new PersonDaoImpl();
        personService = new PersonServiceImpl(personDao);

    }

    @Test
    public void shouldParseValidFile() throws ParseException {

        InputStream inputStream = getClass().getResourceAsStream("/validData.csv");
        EntityBuilder<Person> entityBuilder = new PersonBuilder();
        EntityValidator validator = data -> new ValidationResult();

        List<ValidationMessage> messages = new ArrayList<>();


        FileParser<Person> csvFileParser = new CSVFileParser<>(entityBuilder, validator);
        ParseResult<Person> result = csvFileParser.parse(inputStream);
        List<Person> people = result.getParseResults();
        Assert.assertEquals(10, people.size());
        people.forEach(personService::save);

        List<Person> all = personService.findAll();
        for (Person person : all) {
            Assert.assertNotNull(person.getId());
        }
    }
}
