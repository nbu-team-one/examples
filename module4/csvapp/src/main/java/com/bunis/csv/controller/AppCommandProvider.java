package com.bunis.csv.controller;

public interface AppCommandProvider {

    AppCommand getCommand(String commandName);

    void register(AppCommandName commandName, AppCommand appCommand);
}
