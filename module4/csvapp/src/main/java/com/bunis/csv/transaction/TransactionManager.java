package com.bunis.csv.transaction;

public interface TransactionManager {

    void begin();

    void commit();

    void rollback();
}
