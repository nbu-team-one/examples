package com.bunis.csv.parser;

import com.bunis.csv.builder.EntityBuilder;
import com.bunis.csv.builder.EntityValidator;
import com.bunis.csv.builder.ValidationResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSVFileParser<T> implements FileParser<T> {

    private final EntityBuilder<T> builder;
    private final EntityValidator validator;

    public CSVFileParser(EntityBuilder<T> builder, EntityValidator validator) {
        this.builder = builder;
        this.validator = validator;
    }

    @Override
    public ParseResult<T> parse(InputStream is) throws ParseException {

        List<T> parseResult = new ArrayList<>();
        List<ValidationResult> validationResults = new ArrayList<>();

        try (InputStreamReader reader = new InputStreamReader(is);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            long counter = 0;
            Map<Integer, String> fileHeaders = new HashMap<>();
            while ((line = br.readLine()) != null) {
                if (counter == 0) {
                    //parse header
                    String[] headers = line.split(",");
                    for (int i = 0; i < headers.length; i++) {
                        fileHeaders.put(i, headers[i]);
                    }
                } else {
                    String[] values = line.split(",");
                    Map<String, String> objData = new HashMap<>(values.length);
                    for (int i = 0; i < values.length; i++) {
                        objData.put(fileHeaders.get(i), values[i]);
                    }
                    ValidationResult validationResult = validator.validate(objData);
                    if (validationResult.isValid()) {
                        parseResult.add(builder.build(objData));
                    } else {
                        validationResults.add(validationResult);
                    }
                }
                counter++;
            }
        } catch (IOException e) {
            throw new ParseException(e);
        }
        return new ParseResult<>(parseResult, validationResults);
    }
}
