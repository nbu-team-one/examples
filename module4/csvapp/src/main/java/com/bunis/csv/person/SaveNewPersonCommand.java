package com.bunis.csv.person;

import com.bunis.csv.application.ApplicationConstants;
import com.bunis.csv.builder.EntityBuilder;
import com.bunis.csv.builder.EntityValidator;
import com.bunis.csv.builder.ValidationResult;
import com.bunis.csv.controller.AbstractAppCommand;
import com.bunis.csv.controller.AppCommandName;
import com.bunis.csv.entity.Person;
import com.bunis.csv.entity.PersonField;
import com.bunis.csv.service.PersonService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SaveNewPersonCommand extends AbstractAppCommand {

    private final PersonService personService;

    public SaveNewPersonCommand(PersonService personService) {
        this.personService = personService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        Map<String, String> personData = new HashMap<>();
        for (PersonField personField : PersonField.values()) {
            String parameter = req.getParameter("person." + personField.getFieldName());
            personData.put(personField.getFieldName(), parameter);
        }

        EntityValidator personValidator = new PersonValidator();
        EntityBuilder<Person> personBuilder = new PersonBuilder();

        ValidationResult validate = personValidator.validate(personData);
        Person build = personBuilder.build(personData);
        if (validate.isValid()) {
            personService.save(build);
            String redirect = req.getContextPath() + "?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + AppCommandName.DISPLAY_PERSONS;
            redirect(resp, redirect);
        } else {

            List<String> errorsFromParsing = validate.getMessages().stream()
                    .map(m -> {
                        String fieldName = m.getFieldName();
                        String errors = String.join(",", m.getErrors());
                        return "Field " + fieldName + " contains following errors: " + errors;
                    }).collect(Collectors.toList());
            req.setAttribute("errors", errorsFromParsing);
            req.setAttribute("person", build);
            forward(req, resp, "people");
        }
    }
}
