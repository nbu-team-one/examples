package com.bunis.csv.service;

import com.bunis.csv.dao.PersonDao;
import com.bunis.csv.entity.Person;
import com.bunis.csv.transaction.Transactional;

import java.util.List;

public class PersonServiceImpl implements PersonService {

    private final PersonDao personDao;

    public PersonServiceImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Transactional
    public void save(Person p) {
        this.personDao.save(p);
    }

    public List<Person> findAll() {
        return this.personDao.findAll();
    }
}
