package com.bunis.csv.person;

import com.bunis.csv.builder.EntityBuilder;
import com.bunis.csv.entity.Person;
import com.bunis.csv.entity.PersonField;

import java.util.Map;

public class PersonBuilder implements EntityBuilder<Person> {

    @Override
    public Person build(Map<String, String> data) {
        Person person = new Person();
        for (PersonField field : PersonField.values()) {
            field.getFieldMapper().accept(person, data.get(field.getFieldName()));
        }
        return person;
    }
}
