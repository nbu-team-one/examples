package com.bunis.csv.dao;

import com.bunis.csv.entity.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class PersonDaoImpl implements PersonDao {

    private final Map<Long, Person> storage = new ConcurrentHashMap<>();
    private final AtomicLong sequence = new AtomicLong(0);

    @Override
    public List<Person> findAll() {
        if (this.storage.isEmpty()) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(storage.values());
        }
    }

    @Override
    public void save(Person person) {

        Person value = cloneEntity(person);
        long key = sequence.incrementAndGet();
        value.setId(key);
        this.storage.put(key, value);
    }

    @Override
    public void update(Long key, Person person) {

        if (this.storage.containsKey(key)) {
            this.storage.put(key, cloneEntity(person));
        } else {
            throw new EntityNotFoundException("blabla" + key);
        }
    }

    @Override
    public Person get(Long key) {
        if (this.storage.containsKey(key)) {
            return this.storage.get(key);
        } else {
            throw new EntityNotFoundException("blabla" + key);
        }
    }

    @Override
    public void delete(Long key) {

        this.storage.remove(key);
    }

    private Person cloneEntity(Person person) {

        return person;
    }
}
