package com.bunis.csv.application;

import com.bunis.csv.controller.AbstractAppCommand;
import com.bunis.csv.controller.AppCommand;
import com.bunis.csv.controller.AppCommandName;
import com.bunis.csv.controller.AppCommandProvider;
import com.bunis.csv.controller.SimpleAppCommandProvider;
import com.bunis.csv.dao.PersonDao;
import com.bunis.csv.dao.PersonDaoImpl;
import com.bunis.csv.person.DisplayPersonsCommand;
import com.bunis.csv.person.SaveNewPersonCommand;
import com.bunis.csv.person.UploadPersonsCommand;
import com.bunis.csv.service.PersonService;
import com.bunis.csv.service.PersonServiceImpl;
import com.bunis.csv.transaction.TransactionManager;
import com.bunis.csv.transaction.TransactionManagerImpl;
import com.bunis.csv.transaction.Transactional;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@MultipartConfig
@WebServlet(name = "csv", urlPatterns = "/")
public class AppServlet extends HttpServlet {

    private static final long serialVersionUID = 2604042096773136558L;
    private final AppCommandProvider commandProvider = new SimpleAppCommandProvider();

    static <T> InvocationHandler createTransactionalInvocationHandler(TransactionManager tr, T service) {
        return (proxy, method, args) -> {

            Method declaredMethod = service.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
            if (method.isAnnotationPresent(Transactional.class)
                    || declaredMethod.isAnnotationPresent(Transactional.class)) {
                tr.begin();
                try {
                    Object result = method.invoke(service, args);
                    tr.commit();
                    return result;
                } catch (Exception e) {
                    //log
                    tr.rollback();
                    throw e;
                }
            } else {
                return method.invoke(service, args);
            }
        };
    }

    static <T> T createProxy(ClassLoader classLoader, InvocationHandler handler, Class<T>... toBeProxied) {
        return (T) Proxy.newProxyInstance(classLoader, toBeProxied, handler);
    }

    @Override
    public void init() throws ServletException {

        // create and init connection pool
        // create and init transaction manager
        TransactionManager tr = new TransactionManagerImpl(/*ConnectionPool pool*/);
        // create ConnectionManager and init connection manager new ConnectionManagerImpl(ConnectionPool pool, TransactionManager tr);


        PersonDao dao = new PersonDaoImpl(/*ConnectionManager connectionManager*/);
        PersonService personService = new PersonServiceImpl(dao);

        InvocationHandler transactionalInvocationHandler = createTransactionalInvocationHandler(tr, personService);
        PersonService personServiceProxy = createProxy(getClass().getClassLoader(), transactionalInvocationHandler, PersonService.class);

        AppCommand displayPersonsCommand = new DisplayPersonsCommand(personServiceProxy);
        AppCommand uploadPersonsCommand = new UploadPersonsCommand(personServiceProxy);
        AppCommand saveNewPersonCommand = new SaveNewPersonCommand(personServiceProxy);

        commandProvider.register(AppCommandName.DISPLAY_PERSONS, displayPersonsCommand);
        commandProvider.register(AppCommandName.UPLOAD_PERSONS, uploadPersonsCommand);
        commandProvider.register(AppCommandName.SAVE_NEW_PERSON, saveNewPersonCommand);

        AppCommand defaultCommand = new AbstractAppCommand() {
            @Override
            protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws Exception {
                forward(req, resp, "people");
            }
        };
        commandProvider.register(AppCommandName.DEFAULT_COMMAND, defaultCommand);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String commandName = req.getParameter(ApplicationConstants.COMMAND_NAME_PARAM);
        AppCommand command = this.commandProvider.getCommand(commandName);
        command.execute(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
