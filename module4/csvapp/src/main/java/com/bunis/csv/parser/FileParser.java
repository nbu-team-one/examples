package com.bunis.csv.parser;

import java.io.InputStream;

public interface FileParser<T> {

    ParseResult<T> parse(InputStream is) throws ParseException;
}