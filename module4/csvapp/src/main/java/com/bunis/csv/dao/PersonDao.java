package com.bunis.csv.dao;

import com.bunis.csv.entity.Person;

public interface PersonDao extends CrudEntityDao<Person, Long> {
}
