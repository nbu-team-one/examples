package com.bunis.csv.service;

import com.bunis.csv.entity.Person;

import java.util.List;

public interface PersonService {

    void save(Person p);

    List<Person> findAll();
}
