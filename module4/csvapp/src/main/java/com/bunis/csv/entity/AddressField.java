package com.bunis.csv.entity;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum AddressField {

    STREET("street", Address::setStreet);

    private final String fieldName;
    private final BiConsumer<Address, String> fieldMapper;

    AddressField(String fieldName, BiConsumer<Address, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<AddressField> of(String fieldName) {
        return Stream.of(AddressField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }

    public BiConsumer<Address, String> getFieldMapper() {
        return fieldMapper;
    }
}
