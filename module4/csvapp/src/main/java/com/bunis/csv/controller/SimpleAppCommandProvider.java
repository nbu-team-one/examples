package com.bunis.csv.controller;

import java.util.EnumMap;
import java.util.Map;

public class SimpleAppCommandProvider implements AppCommandProvider {

    private final Map<AppCommandName, AppCommand> commandMap = new EnumMap<>(AppCommandName.class);

    @Override
    public AppCommand getCommand(String commandName) {
        return commandMap.get(AppCommandName.of(commandName));
    }

    @Override
    public void register(AppCommandName commandName, AppCommand appCommand) {
        this.commandMap.put(commandName, appCommand);
    }
}
