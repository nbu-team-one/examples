package com.bunis.csv.entity;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public enum PersonField {

    EMAIL("email", Person::setEmail),
    FIRST_NAME("firstName", Person::setFirstName),
    LAST_NAME("lastName", Person::setLastName),
    LANGUAGE("lang", Person::setLang),
    GENDER("gender", (person, s) -> person.setGender(PersonGender.fromString(s)));

    private final String fieldName;
    private final BiConsumer<Person, String> fieldMapper;

    PersonField(String fieldName, BiConsumer<Person, String> fieldMapper) {
        this.fieldName = fieldName;
        this.fieldMapper = fieldMapper;
    }

    public static Optional<PersonField> of(String fieldName) {
        return Stream.of(PersonField.values()).filter(f -> f.fieldName.equals(fieldName)).findFirst();
    }

    public String getFieldName() {
        return fieldName;
    }

    public BiConsumer<Person, String> getFieldMapper() {
        return fieldMapper;
    }
}
