package com.bunis.csv.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AppCommand {

    void execute(HttpServletRequest req, HttpServletResponse resp);
}
