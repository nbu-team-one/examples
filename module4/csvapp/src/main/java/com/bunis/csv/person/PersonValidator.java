package com.bunis.csv.person;

import com.bunis.csv.builder.EntityValidator;
import com.bunis.csv.builder.ValidationMessage;
import com.bunis.csv.builder.ValidationResult;
import com.bunis.csv.entity.PersonField;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonValidator implements EntityValidator {
    @Override
    public ValidationResult validate(Map<String, String> data) {
        List<ValidationMessage> validationMessages = Stream.of(PersonField.values())
                .filter(f -> !data.containsKey(f.getFieldName())
                        || data.get(f.getFieldName()) == null
                        || data.get(f.getFieldName()).trim().length() == 0)
                .map(f -> new ValidationMessage(f.getFieldName(), null, Collections.singletonList("missed required field")))
                .collect(Collectors.toList());

        ValidationResult validationResult = new ValidationResult();
        validationResult.addMessages(validationMessages);
        return validationResult;
    }
}
