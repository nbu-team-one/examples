package com.bunis.csv.parser;

import com.bunis.csv.builder.ValidationResult;

import java.util.List;

public class ParseResult<T> {

    private List<T> parseResults;
    private List<ValidationResult> validationResults;

    public ParseResult(List<T> parseResults, List<ValidationResult> validationResults) {
        this.parseResults = parseResults;
        this.validationResults = validationResults;
    }

    public List<T> getParseResults() {
        return parseResults;
    }

    public List<ValidationResult> getValidationResults() {
        return validationResults;
    }

    public boolean success() {
        return this.validationResults == null || this.validationResults.isEmpty();
    }
}
