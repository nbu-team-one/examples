package com.bunis.csv.controller;

import java.util.stream.Stream;

public enum AppCommandName {

    DISPLAY_PERSONS,
    UPLOAD_PERSONS,
    SAVE_NEW_PERSON,
    DEFAULT_COMMAND;

    public static AppCommandName of(String commandName) {

        return Stream.of(AppCommandName.values())
                .filter(c -> c.name().equalsIgnoreCase(commandName))
                .findFirst().orElse(DEFAULT_COMMAND);
    }
}
