package com.bunis.csv.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractAppCommand implements AppCommand {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractAppCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        try {
            executeWrapped(req, resp);
        } catch (Exception e) {
            LOGGER.error("Failed to execute command " + this.getClass().getName(), e);
        }
    }

    protected abstract void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws Exception;

    protected void forward(HttpServletRequest req, HttpServletResponse resp, String viewName) {

        try {
            req.setAttribute("viewName", viewName);
            req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to forward view", e);
        }
    }

    protected void redirect(HttpServletResponse resp, String redirect) {
        try {
            resp.sendRedirect(redirect);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to redirect", e);
        }
    }
}
