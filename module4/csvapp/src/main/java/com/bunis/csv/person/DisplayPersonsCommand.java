package com.bunis.csv.person;

import com.bunis.csv.controller.AbstractAppCommand;
import com.bunis.csv.service.PersonService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayPersonsCommand extends AbstractAppCommand {

    private PersonService personService;

    public DisplayPersonsCommand(PersonService personService) {
        this.personService = personService;
    }


    @Override
    public void executeWrapped(HttpServletRequest req, HttpServletResponse resp) {

        req.setAttribute("people", personService.findAll());
        forward(req, resp, "people");
    }
}
