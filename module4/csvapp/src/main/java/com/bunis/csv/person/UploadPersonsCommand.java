package com.bunis.csv.person;

import com.bunis.csv.application.ApplicationConstants;
import com.bunis.csv.builder.EntityValidator;
import com.bunis.csv.controller.AbstractAppCommand;
import com.bunis.csv.controller.AppCommandName;
import com.bunis.csv.entity.Person;
import com.bunis.csv.parser.CSVFileParser;
import com.bunis.csv.parser.FileParser;
import com.bunis.csv.parser.ParseException;
import com.bunis.csv.parser.ParseResult;
import com.bunis.csv.service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class UploadPersonsCommand extends AbstractAppCommand {

    private final PersonService personService;

    public UploadPersonsCommand(PersonService personService) {
        this.personService = personService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        Part csvFile = req.getPart("csvFile");

        List<Part> csvFileList = req.getParts()
                .stream()
                .filter(p -> p.getName().equals("csvFile"))
                .collect(Collectors.toList());

        csvFileList.forEach(f-> LOGGER.info("Received file: {}", f.getSubmittedFileName()));

        if (csvFile == null || csvFile.getSize() <= 0L) {
            req.setAttribute("errors", Collections.singleton("File doesn't exist or empty"));
            forward(req, resp, "people");
            return;
        }
        if (!"text/csv".equalsIgnoreCase(csvFile.getContentType())) {
            req.setAttribute("errors", Collections.singleton("File is not a CSV format"));
            forward(req, resp, "people");
            return;
        }

        try (InputStream inputStream = csvFile.getInputStream()) {

            EntityValidator entityValidator = new PersonValidator();
            FileParser<Person> parser = new CSVFileParser<>(new PersonBuilder(), entityValidator);
            ParseResult<Person> result = parser.parse(inputStream);
            if (result.success()) {
                List<Person> people = result.getParseResults();
                people.forEach(personService::save);
                String redirect = req.getContextPath() + "?" + ApplicationConstants.COMMAND_NAME_PARAM + "=" + AppCommandName.DISPLAY_PERSONS;
                redirect(resp, redirect);
            } else {
                List<String> errorsFromParsing = result.getValidationResults().stream()
                        .flatMap(vr -> vr.getMessages().stream())
                        .map(m -> {
                            String fieldName = m.getFieldName();
                            String errors = String.join(",", m.getErrors());
                            return "Field " + fieldName + " contains following errors: " + errors;
                        }).collect(Collectors.toList());
                req.setAttribute("errors", errorsFromParsing);
                forward(req, resp, "people");
            }
        } catch (ParseException e) {
            LOGGER.error("Cannot upload file", e);
            req.setAttribute("errors", Collections.singleton("Failed to parse file"));
            forward(req, resp, "people");
        }

    }
}
