package com.bunis.csv.validation;

public class ValidationRule {

    private final int maxLength;
    private final boolean isMandatory;

    public ValidationRule(int maxLength, boolean isMandatory) {
        this.maxLength = maxLength;
        this.isMandatory = isMandatory;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public boolean isMandatory() {
        return isMandatory;
    }
}
