package com.bunis.csv.builder;

import java.util.Map;

public interface EntityValidator {

    ValidationResult validate(Map<String, String> data);
}
