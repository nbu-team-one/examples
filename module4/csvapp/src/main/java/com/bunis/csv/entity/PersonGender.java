package com.bunis.csv.entity;

import java.util.stream.Stream;

public enum PersonGender {

    M("Male", 1),
    F("Female", 2),
    U("Undefined", 3);

    private final String genderName;
    private final int genderCode;

    PersonGender(String genderName, int genderCode) {
        this.genderName = genderName;
        this.genderCode = genderCode;
    }

    public String getGenderName() {
        return genderName;
    }

    public static PersonGender fromString(String search) {

        return Stream.of(PersonGender.values())
                .filter(g -> g.genderName.equalsIgnoreCase(search))
                .findFirst()
                .orElse(U);
    }
}
