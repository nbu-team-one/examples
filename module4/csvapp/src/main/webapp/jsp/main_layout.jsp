
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.bunis.csv.controller.AppCommandName" %>
<%@ page import="com.bunis.csv.application.ApplicationConstants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/jsp/error.jsp" %>
<html>
<head>
    <title>CSV Import App</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bulma.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/validation.js"></script>
</head>
<body>

<section class="hero is-dark">
    <div class="hero-head">

    </div>
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                CSV IMPORT
            </h1>
            <h2 class="subtitle">
                EXAMPLE OF CSV PARSER
            </h2>
        </div>
    </div>
</section>
<section class="is-fullheight is-medium">
    <div class="columns">
        <div class="column is-one-quarter">
            <aside class="menu">
                <p class="menu-label">
                    NAVIGATION
                </p>
                <ul class="menu-list">
                    <li>
                        <a href="?${ApplicationConstants.COMMAND_NAME_PARAM}=${AppCommandName.DISPLAY_PERSONS}">
                            SHOW PERSONS
                        </a>
                    </li>
                </ul>
            </aside>
        </div>
        <div class="column is-three-quarters">
            <div class="container">
                <div class="content">
                    <jsp:include page="view/${viewName}.jsp"/>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="content has-text-centered">
        <p>
            THANKS
        </p>
    </div>
</section>
</body>
</html>
