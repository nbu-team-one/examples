<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.bunis.csv.application.ApplicationConstants" %>
<%@ page import="com.bunis.csv.controller.AppCommandName" %>
<%@ page import="com.bunis.csv.entity.PersonField" %>
<%@ page import="com.bunis.csv.entity.AddressField" %>
<%@ page import="com.bunis.csv.entity.PersonGender" %>

<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form" %>

<script type="text/javascript">


</script>

<c:if test="${not empty errors}">
    <c:forEach items="${errors}" var="error">
        <article class="message is-danger is-small">
            <div class="message-body">
                    ${error}
            </div>
        </article>
    </c:forEach>
</c:if>

<jsp:useBean id="person" scope="request" class="com.bunis.csv.entity.Person"/>
<jsp:useBean id="address" scope="request" class="com.bunis.csv.entity.Address"/>
<div class="column is-three-quarters">

    <form id="person.form" action="${pageContext.request.contextPath}/" method="post"
          onsubmit="return validateBeforeSubmit('person.form');">

        <form:input beanName="person" fieldName="${PersonField.EMAIL.fieldName}" isMandatory="true" pattern="'some+regular+expression'"/>
        <form:input beanName="person" fieldName="${PersonField.FIRST_NAME.fieldName}" isMandatory="true" minLength="5"/>
        <form:input beanName="person" fieldName="${PersonField.LAST_NAME.fieldName}" isMandatory="true" minLength="2" maxLength="10"/>
        <form:input beanName="person" fieldName="${PersonField.LANGUAGE.fieldName}" isMandatory="true" maxLength="2" minLength="2"/>

        <div class="field">
            <div class="control">
                <div class="select">
                    <select name="person.${PersonField.GENDER.fieldName}">
                        <c:forEach items="${PersonGender.values()}" var="gender">
                            <option value="${gender.genderName}">${gender.genderName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>


        <input type="hidden" name="${ApplicationConstants.COMMAND_NAME_PARAM}"
               value="${AppCommandName.SAVE_NEW_PERSON}">
        <div class="field is-grouped">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
        </div>
    </form>

    <form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data">

        <form:input beanName="address" fieldName="${AddressField.STREET.fieldName}" validate="false"/>

        <div class="field">
            <div id="file-js-example" class="file has-name is-primary">
                <label class="file-label">
                    <input class="file-input" type="file" multiple name="csvFile">
                    <span class="file-cta">
          <span class="file-icon">
            <i class="fas fa-upload"></i>
          </span>
          <span class="file-label">
            Choose a file…
          </span>
        </span>
                    <span class="file-name">
          No file uploaded
        </span>
                </label>
            </div>
        </div>
        <input type="hidden" name="${ApplicationConstants.COMMAND_NAME_PARAM}" value="${AppCommandName.UPLOAD_PERSONS}">
        <div class="field is-grouped">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
        </div>
    </form>
    <c:if test="${not empty people}">
        <table class="table is-striped is-hoverable is-narrow is-fullwidth">
            <thead>

            <th><abbr title="First Name">First Name</abbr></th>
            <th><abbr title="Last Name">Last Name</abbr></th>
            <th><abbr title="Email">Email</abbr></th>
            <th><abbr title="Language">Language</abbr></th>
            <th><abbr title="Gender">Gender</abbr></th>
            </thead>
            <tbody>
            <c:forEach items="${people}" var="person">
                <tr>
                    <td><c:out value="${person.firstName}"/></td>
                    <td><c:out value="${person.lastName}"/></td>
                    <td><c:out value="${person.email}"/></td>
                    <td><c:out value="${person.lang}"/></td>
                    <td><c:out value="${person.gender}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>
<script type="text/javascript">
    const fileInput = document.querySelector('#file-js-example input[type=file]');
    fileInput.onchange = () => {
        if (fileInput.files.length > 0) {
            const fileName = document.querySelector('#file-js-example .file-name');
            fileName.textContent = fileInput.files[0].name;
        }
    }
</script>