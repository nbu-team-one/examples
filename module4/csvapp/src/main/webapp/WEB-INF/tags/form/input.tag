<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:directive.attribute name="fieldName" required="true" description="field name to display"/>
<jsp:directive.attribute name="beanName" required="true" description="some object"/>

<jsp:directive.attribute name="validate" type="java.lang.Boolean"/>
<jsp:directive.attribute name="minLength" type="java.lang.Integer"/>
<jsp:directive.attribute name="maxLength" type="java.lang.Integer"/>
<jsp:directive.attribute name="pattern" type="java.lang.String"/>
<jsp:directive.attribute name="isMandatory" type="java.lang.Boolean"/>

<c:if test="${validate == null}">
    <c:set value="true" var="validate"/>
</c:if>

<c:if test="${isMandatory == null}">
    <c:set value="false" var="isMandatory"/>
</c:if>

<c:if test="${minLength == null}">
    <c:set value="undefined" var="minLength"/>
</c:if>
<c:if test="${maxLength == null}">
    <c:set value="undefined" var="maxLength"/>
</c:if>
<c:if test="${pattern == null}">
    <c:set value="undefined" var="pattern"/>
</c:if>

<c:if test="${not empty fieldName and not empty beanName}">

    <div class="field">

        <c:set var="fieldPath" value="${beanName}.${fieldName}"/>
        <c:set var="bean" value="${requestScope[beanName]}"/>
        <c:out value="${bean[fieldName]}"/>
        <div class="control">
            <input id="${fieldPath}"
                   class="input is-info" type="text"
                   name="${fieldPath}"
                   value="${bean[fieldName]}"
                   placeholder="${fieldPath}"/>
        </div>
        <c:if test="${validate}">
            <script type="text/javascript">
                subscribeToValidate('${beanName}.form', '${fieldPath}', {
                    'minLength': ${minLength},
                    'maxLength': ${maxLength},
                    'pattern': ${pattern},
                    'isMandatory': ${isMandatory},
                });
            </script>
        </c:if>
    </div>


</c:if>
