let forms = {};

function subscribeToValidate(formId, inputId, validationConfig) {

    if (!forms[formId]) {
        forms[formId] = [];
    }

    let input = document.getElementById(inputId);
    input.onkeyup = function () {
        doValidate(input, validationConfig);
    }

    forms[formId].push({
        'inputId': inputId,
        'input': input,
        'validationConfig': validationConfig
    });
}

function doValidate(input, validationConfig) {
    let value = input.value;

    if (!validationConfig) {
        input.classList.value = 'input is-primary';
        return true;
    }

    let isValid = true;

    if (validationConfig.isMandatory && !value) {
        isValid = false;
    }

    if (validationConfig.maxLength && value && value.length > validationConfig.maxLength) {
        isValid = false;
    }
    if (validationConfig.minLength && value && value.length < validationConfig.minLength) {
        isValid = false;
    }

    if (isValid) {
        input.classList.value = 'input is-primary';
        return true;
    } else {
        input.classList.value = 'input is-danger';
        return false;
    }
}

function validateBeforeSubmit(formId) {
    let inputs = forms[formId];
    let validForm = true;
    for (let input of inputs) {
        if (!doValidate(input.input, input.validationConfig)) {
            validForm = false;
        }
    }
    if (!validForm) {
        alert('Fix all errors');
    }
    return validForm;
}