package com.bunis.concurrency.app;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceTest {

    @Test
    public void shouldRunForever() throws InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        for (int i = 0; i < 10; i++) {
            executorService.submit(new Task(i + "name"));
        }

        executorService.awaitTermination(20, TimeUnit.SECONDS);
    }


    @Test
    public void executeScheduled() {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        Future<String> future = executorService.schedule(() -> {
            // do some staff
//            LOGGER.info("message from future task");
            return "Hello world";
        }, 1, TimeUnit.SECONDS);

        ScheduledFuture<?> scheduledFuture = executorService.schedule(() -> {
            // do some staff
//            LOGGER.info("message from void future task");
        }, 1, TimeUnit.SECONDS);

        executorService.shutdown();
    }


    @Test
    public void completeFuture() {


        CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {
            return "sdfg";
        });
    }

}
