package com.bunis.concurrency.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
@RunWith(JUnit4.class)
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void logingQueueFulfilled() throws InterruptedException {

        int availableSlots = 10;
        ExecutorService service = Executors.newFixedThreadPool(availableSlots);

        LoginQueueUsingSemaphore queueUsingSemaphore = new LoginQueueUsingSemaphore(availableSlots);
        IntStream.range(0, availableSlots + 5).forEach(login -> {
            Runnable command = () -> {
                queueUsingSemaphore.login();
                try {
                    Thread.currentThread().sleep(10_000L);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            };
            service.execute(command);

//            queueUsingSemaphore.logout();
        });
//        Thread.sleep(10_000L);

//        assertEquals(0, queueUsingSemaphore.availableSlots());
//        assertTrue(queueUsingSemaphore.tryLogin());

        System.out.println("Available: " + queueUsingSemaphore.availableSlots());

//        assertTrue(queueUsingSemaphore.availableSlots() > 0);
        assertTrue(queueUsingSemaphore.tryLogin());

        service.shutdown();
    }
}
