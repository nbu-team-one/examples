package com.bunis.concurrency.app;

import org.apache.log4j.Logger;

public class Task implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Task.class);
    private static final int MAX_COUNT = 5;

    private final String name;

    private int count;

    public Task(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        // task logic
        while (count <= MAX_COUNT) {
            LOGGER.info("doing task " + name + ": " + count);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            count++;
        }

    }
}