package com.bunis.concurrency.app;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;

import org.apache.log4j.Logger;

public class Bus {

    private static final Logger LOGGER = Logger.getLogger(Bus.class);
    // Immediately register the main thread
    // Phases 0 and 6 - this is a bus depot, 1 - 5 stops
    private static final Phaser PHASER = new Phaser(1);

    public static void main(String[] args) throws InterruptedException {

        List<Passenger> passengers = new ArrayList<>();
        // Generate passengers at bus stops
        for (int i = 1; i < 5; i++) {
            if ((int) (Math.random() * 2) > 0) {
                // This passenger goes to the next one.
                passengers.add(new Passenger(i, i + 1, PHASER));
            }
            if ((int) (Math.random() * 2) > 0) {
                // This passenger goes to the final
                passengers.add(new Passenger(i, 5, PHASER));
            }
        }
        for (int i = 0; i < 7; i++) {
            switch (i) {
            case 0:
                LOGGER.info("The bus left the park.");
                // In phase 0, only 1 participant - bus
                PHASER.arrive();
                break;
            case 6:
                LOGGER.info("The bus went to the park.");
                // Remove the main stream, break the barrier
                PHASER.arriveAndDeregister();
                break;
            default:
                int currentBusStop = PHASER.getPhase();
                LOGGER.info("Bus stop #" + currentBusStop);
                // Check if there are passengers at the bus stop
                for (Passenger p : passengers) { 
                    if (p.getDeparture() == currentBusStop) {
                        // Register the stream that will participate in the phases
                        PHASER.register();
                        p.start(); // and starting
                    }
                }
                // Report your readiness
                PHASER.arriveAndAwaitAdvance();
            }
        }
    }
}