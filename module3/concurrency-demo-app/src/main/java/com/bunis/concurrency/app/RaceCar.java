package com.bunis.concurrency.app;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

public class RaceCar implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(RaceCar.class);

    private final int carNumber;
    // consider that the vehicle speed is constant
    private final int carSpeed;
    private final int trackLength;
    private final CountDownLatch latch;

    public RaceCar(int carNumber, int carSpeed, int trackLength, CountDownLatch latch) {
        this.carNumber = carNumber;
        this.carSpeed = carSpeed;
        this.latch = latch;
        this.trackLength = trackLength;
    }

    @Override
    public void run() {
        try {

            LOGGER.info("Car " + carNumber + " is ready");
            // The car drove up to the starting straight line - the condition is fulfilled
            // decrement the counter by 1
            latch.countDown();
            // the await () method blocks the thread that called it, until
            // counter CountDownLatch will not become equal to 0
            latch.await();
            // we wait while will pass the route
            Thread.sleep(trackLength / carSpeed);
            LOGGER.info("Car " + carNumber + " was finised");
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }
}