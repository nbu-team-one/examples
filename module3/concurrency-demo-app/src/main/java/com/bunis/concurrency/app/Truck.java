package com.bunis.concurrency.app;

import org.apache.log4j.Logger;

import java.util.concurrent.Exchanger;

import static java.text.MessageFormat.format;

public class Truck implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Truck.class);

    private final int number;
    private final String dep;
    private final String dest;
    private final String[] parcels;
    private final Exchanger<String> exchanger;

    public Truck(int number, String departure, String destination, String[] parcels, Exchanger<String> exchanger) {
        this.number = number;
        this.dep = departure;
        this.dest = destination;
        this.parcels = parcels;
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        try {
            String loadMessage = "In the truck #{0} loaded: {1} and {2}.";
            LOGGER.info(format(loadMessage, number, parcels[0], parcels[1]));
            String awayMessage = "Truck #{0} went from {1} to {2}.";
            LOGGER.info(format(awayMessage, number, dep, dest));
            Thread.sleep(1000 + (long) Math.random() * 5000);
            String ariveMessage = "Truck #{0} arrived in Е.";
            LOGGER.info(format(ariveMessage, number));
            // When calling exchange (), the thread is blocked and waits
            // while another thread calls exchange (), then the parcels will be exchanged
            parcels[1] = exchanger.exchange(parcels[1]);
            String moveParcelMessage = "In the truck {0} moved the parcel for item {1}.";
            LOGGER.info(format(moveParcelMessage, number, dest));
            Thread.sleep(1000 + (long) Math.random() * 5000);
            String deliveryMessage = "Truck #{0} arrived at {1} and delivered: {2} and {3}";
            LOGGER.info(format(deliveryMessage, number, dest, parcels[0], parcels[1]));
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }
}