package com.bunis.concurrency.app;

import java.util.concurrent.Exchanger;

public class Delivery {

    // Create an exchanger that will exchange the type String
    private static final Exchanger<String> EXCHANGER = new Exchanger<>();

    public static void main(String[] args) throws InterruptedException {
        // Generate cargo for the 1st truck
        String[] p1 = new String[]{"{package A->D}", "{package A->C}"};
        // Generate cargo for the 2nd truck
        String[] p2 = new String[]{"{package B->C}", "{package B->D}"};
        // Ship 1st truck from A to D
        new Thread(new Truck(1, "A", "D", p1, EXCHANGER)).start();
        Thread.sleep(1_000);
        // Ship 2nd truck from B to C
        new Thread(new Truck(2, "B", "C", p2, EXCHANGER)).start();
    }
}