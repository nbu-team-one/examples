package com.bunis.concurrency.ferry;

public class Car extends Thread {
    public int weight;
    int carState = 0;
    int id;
    Ferry ferry;

    public Car(Ferry f, int weight) {
        this.ferry = f;
        this.weight = weight * 10;
        this.id = weight;
    }

    public void waitForDepart() {
        System.out.println("Загрузились машина" + id);
        this.carState = 1;

    }

    public void depart() {
        System.out.println("Выгрузились машина" + id);
        this.carState = 2;
    }

    @Override
    public void run() {

        while (this.carState != 2) {
            System.out.println("Патаюсь загрузится " + id);
            boolean load = ferry.tryLoad(this);
            if (!load) {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
