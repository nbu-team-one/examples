package com.bunis.concurrency.ferry;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FerryRunner {

    public static void main(String[] args) {
        Ferry ferry = new Ferry();
        ferry.start();
        ExecutorService runner = Executors.newFixedThreadPool(6);
        for (int i = 0; i < 6; i++) {
            runner.submit(new Car(ferry, i + 1));
        }
    }
}
