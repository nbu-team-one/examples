package com.bunis.concurrency.app;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App {
    private static final Logger LOGGER = Logger.getLogger(App.class);

    public static void main(String[] args) {

//        Executor executor = new Invoker();
//        executor.execute(() -> {
//            LOGGER.info("message from executor");
//        });

        App app = new App();
        app.runCyclic();
    }

    public void runCyclic() {

        // Initialize the barrier into three streams and tasks, which will be executed
        // when
        // at the barrier there will be three streams. After that, they will be
        // released.
        CyclicBarrier barrier = new CyclicBarrier(4, () -> {
            // Task to be executed when the sides reach the barrier
            LOGGER.info("All tasks are completed");
        });

        Thread t1 = new Thread(new CyclicTask(barrier), "t1");
        Thread t2 = new Thread(new CyclicTask(barrier), "t2");
        Thread t3 = new Thread(new CyclicTask(barrier), "t3");
        Thread t4 = new Thread(new CyclicTask(barrier), "t4");

        if (!barrier.isBroken()) {
            t1.start();
            t2.start();
            t3.start();
            t4.start();
        }
    }

    public void runFututre() {

        ExecutorService service = Executors.newFixedThreadPool(2);
        Future<String> future = service.submit(() -> {
            // do some staff
            Thread.sleep(10000l);
            return "Hello world";
        });

        try {
            if (future.isDone() && !future.isCancelled()) {
                LOGGER.info(future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.error(e);
        }
    }

    public void runTasks() {

        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(new Task("name"));
        service.submit(() -> {
            new Task("name");
        });
        service.shutdown();
        try {
            service.awaitTermination(20L, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }

    public void executeScheduled() {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        executorService.scheduleWithFixedDelay(() -> {
            // do some staff
            LOGGER.info("message from future task");
        }, 1, 15, TimeUnit.SECONDS);

        executorService.scheduleAtFixedRate(() -> {
            // do some staff
            LOGGER.info("message from void future task");
        }, 1, 20, TimeUnit.SECONDS);

        // executorService.shutdown();
    }
}
