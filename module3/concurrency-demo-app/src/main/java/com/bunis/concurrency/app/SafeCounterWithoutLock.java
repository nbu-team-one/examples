package com.bunis.concurrency.app;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class SafeCounterWithoutLock {
    private final AtomicInteger counter = new AtomicInteger(0);
    ReentrantLock lock = new ReentrantLock();
    Condition c = lock.newCondition();
    public int getValue() {
        return counter.get();
    }
    public void increment() {
        while(true) {
            int existingValue = getValue();
            int newValue = existingValue + 1;
            
            if(counter.compareAndSet(existingValue, newValue)) {
                return;
            }
        }
    }
 }
 