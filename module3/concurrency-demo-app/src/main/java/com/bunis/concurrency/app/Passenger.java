package com.bunis.concurrency.app;

import java.util.concurrent.Phaser;

import org.apache.log4j.Logger;

public class Passenger extends Thread {

    private static final Logger LOGGER = Logger.getLogger(Passenger.class);

    private final int departure;
    private final int destination;
    private final Phaser pahser;

    public Passenger(int departure, int destination, Phaser phaser) {
        this.departure = departure;
        this.destination = destination;
        this.pahser = phaser;
        LOGGER.info(this + " is waiting at the bus stop # " + this.departure);
    }

    @Override
    public void run() {
        try {
            LOGGER.info(this + " got on the bus.");
            while (pahser.getPhase() < destination) {
                // Until the bus arrives at the desired stop (phase)
                pahser.arriveAndAwaitAdvance();
                // declare readiness in each phase and wait
            }
            Thread.sleep(1);
            LOGGER.info(this + " leaves the bus.");
            // Cancel registration on the desired phase
            pahser.arriveAndDeregister(); 
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public String toString() {
        return "Passenger{" + departure + " -> " + destination + '}';
    }

    public int getDeparture() {
        return departure;
    }

    public int getDestination() {
        return destination;
    }
}