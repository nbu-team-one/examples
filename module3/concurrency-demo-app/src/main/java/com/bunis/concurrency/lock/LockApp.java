package com.bunis.concurrency.lock;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class LockApp {

    private static final Logger LOGGER = Logger.getLogger(LockApp.class);
    private static final SynchronizedHashMapWithReadWriteLock SYNC_MAP = new SynchronizedHashMapWithReadWriteLock();

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        List<Runnable> tasks = new ArrayList<>();
        tasks.add(() -> {
            for (int i = 0; i < 5; i++) {
                String key = "key" + i;
                SYNC_MAP.put(key, "value from " + Thread.currentThread().getName());
                LOGGER.info("put key " + key);
                try {
                    Thread.sleep(1_000L);
                } catch (InterruptedException e) {
                    LOGGER.error(e);
                }
            }
        });
        tasks.add(() -> {
            for (int i = 0; i < 5; i++) {
                String key = "key" + i;
                if (SYNC_MAP.containsKey(key)) {
                    SYNC_MAP.remove(key);
                    LOGGER.info("removed value fro key " + key);
                } else {
                    SYNC_MAP.put(key, "value from " + Thread.currentThread().getName());
                    LOGGER.info("put key " + key);
                }
                try {
                    Thread.sleep(1_000L);
                } catch (InterruptedException e) {
                    LOGGER.error(e);
                }
            }
        });
        tasks.add(() -> {
            try {
                Thread.sleep(10L);
            } catch (InterruptedException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            for (int i = 0; i < 5; i++) {
                String key = "key" + i;
                String value = SYNC_MAP.get(key);
                String message = MessageFormat.format("Got \n\t'key':{0}\n\t'value':{1}", key, value);
                LOGGER.info(message);
                try {
                    Thread.sleep(1_110L);
                } catch (InterruptedException e) {
                    LOGGER.error(e);
                }
            }
        });
        tasks.forEach(executorService::submit);

        try {
            executorService.awaitTermination(10L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        } finally {
            executorService.shutdownNow();
        }
    }
}