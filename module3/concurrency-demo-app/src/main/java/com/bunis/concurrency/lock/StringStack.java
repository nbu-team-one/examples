package com.bunis.concurrency.lock;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class StringStack {

    private static final Logger LOG = Logger.getLogger(StringStack.class);

    Queue<String> stack = new LinkedList<>();
    int CAPACITY = 5;

    ReentrantLock lock = new ReentrantLock();
    Condition stackEmptyCondition = lock.newCondition();
    Condition stackFullCondition = lock.newCondition();

    public void pushToStack(String item) throws InterruptedException {
        try {
            lock.lock();
            while (stack.size() == CAPACITY) {
                stackFullCondition.await();
                LOG.info("Some space released");
            }
            stack.add(item);
            stackEmptyCondition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public String popFromStack() throws InterruptedException {
        try {
            lock.lock();
            while (stack.isEmpty()) {
                stackEmptyCondition.await();
                LOG.info("Some messages added");
            }
            return stack.remove();
        } finally {
            stackFullCondition.signalAll();
            lock.unlock();
        }
    }
}