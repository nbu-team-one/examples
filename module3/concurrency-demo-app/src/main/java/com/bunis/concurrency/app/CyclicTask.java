package com.bunis.concurrency.app;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import org.apache.log4j.Logger;

public class CyclicTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(CyclicTask.class);

    private CyclicBarrier barrier;

    public CyclicTask(CyclicBarrier barrier) {
        this.barrier = barrier;
    }

    @Override
    public void run() {
        try {
            LOGGER.info(Thread.currentThread().getName() + " is waiting...");
            // To tell the thread that it has reached the barrier, you need to call the
            // await () method
            // After that, this thread is blocked, and waits until the other parties reach
            // the barrier
            barrier.await();
            LOGGER.info(Thread.currentThread().getName() + " released");
        } catch (InterruptedException | BrokenBarrierException e) {
            LOGGER.error(e);
        }
    }
}