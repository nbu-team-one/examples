package com.bunis.concurrency.lock;

import org.apache.log4j.Logger;

public class ReadWriteLockDemo {
    private static final Logger LOG = Logger.getLogger(LockApp.class);


    public static void main(String[] args) {

        StringStack stringStack = new StringStack();


        Runnable readRunnable = () -> {

            for (int i = 0; i < 10; i++) {
                try {
                    String s = stringStack.popFromStack();
                    LOG.info("READ FROM POOL: " + s);
                } catch (InterruptedException e) {
                    throw new IllegalThreadStateException(e.getMessage());
                }
            }
        };

        Runnable writeRunnable = () -> {

            String name = Thread.currentThread().getName();
            for (int i = 0; i < 10; i++) {
                try {
                    LOG.info("WRITE POOL: message" + i + name);
                    stringStack.pushToStack("message" + i + name);
                } catch (InterruptedException e) {
                    throw new IllegalThreadStateException(e.getMessage());
                }
            }
        };

        for (int i = 0; i < 5; i++) {
            new Thread(readRunnable, "READ" + i).start();
            new Thread(writeRunnable, "WRITE" + i).start();
        }
    }


}
