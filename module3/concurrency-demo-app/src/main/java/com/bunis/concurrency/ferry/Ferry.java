package com.bunis.concurrency.ferry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Ferry extends Thread {

    private int maxIteration = 5;
    private int currentIteration = 0;
    private int maxLoad = 61;
    private volatile int currentLoad = 0;
    private volatile boolean loaded = false;
    private Lock loadLock = new ReentrantLock();
    private List<Car> cars = new ArrayList<>();

    public boolean tryLoad(Car car) {

        if (!loaded && loadLock.tryLock()) {
            try {
                if (currentLoad + car.weight < maxLoad) {
                    car.waitForDepart();
                    cars.add(car);
                    currentLoad = currentLoad + car.weight;
                    return true;
                } else {
                    loaded = true;
                    return false;
                }
            } finally {
                loadLock.unlock();
            }
        } else {
            return false;
        }
    }

    private void depart() {

        System.out.println("Приплыли итерация" + currentIteration);
        for (Car car : cars) {
            car.depart();
        }
        cars.clear();
        loadLock.lock();
        loaded = false;
        currentLoad = 0;
        currentIteration++;
        loadLock.unlock();
    }

    @Override
    public void run() {
        while (currentIteration < maxIteration) {
            System.out.println("Ждем.... итерация" + currentIteration);
            if (!loaded) {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            } else {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
                depart();
            }
        }

    }
}
