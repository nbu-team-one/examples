package com.bunis.concurrency.app;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

public class Race {

    private static final Logger LOGGER = Logger.getLogger(Race.class);

    // Create CountDownLatch for 8 'conditions'
    private static final CountDownLatch START = new CountDownLatch(8);

    // The notional length of the race track
    private static final int trackLength = 500_000;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 1; i <= 5; i++) {
            new Thread(new RaceCar(i, (int) (Math.random() * 100 + 50), trackLength, START)).start();
            Thread.sleep(1000);
        }

        while (START.getCount() > 3) {
            // Check if all the cars together
            // at the starting line. If not, wait 100ms
            Thread.sleep(100);
        }

        Thread.sleep(1000);
        LOGGER.info("Ready");
        // Command given, decrement counter by 1
        START.countDown();
        Thread.sleep(1000);
        LOGGER.info("Stady");
        // Command given, decrement counter by 1
        START.countDown();
        Thread.sleep(1000);
        LOGGER.info("Go!");
        START.countDown();
        // Command given, decrement counter by 1
        // the counter becomes zero, and all the waiting threads
        // unlock simultaneously
    }
}