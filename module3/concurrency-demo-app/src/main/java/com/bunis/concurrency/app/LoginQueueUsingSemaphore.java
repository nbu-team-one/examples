package com.bunis.concurrency.app;

import java.util.concurrent.Semaphore;

public class LoginQueueUsingSemaphore {

    private Semaphore semaphore;

    public LoginQueueUsingSemaphore(int slotLimit) {
        semaphore = new Semaphore(slotLimit);
    }

    public boolean tryLogin() {
        return semaphore.tryAcquire();
    }

    public void login() {

        try {
            semaphore.acquire(2);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

    }

    public void logout() {
        semaphore.release();
    }

    public int availableSlots() {
        return semaphore.getQueueLength();
    }
}