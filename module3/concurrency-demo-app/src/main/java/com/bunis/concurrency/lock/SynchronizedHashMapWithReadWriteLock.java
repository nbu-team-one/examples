package com.bunis.concurrency.lock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.log4j.Logger;

public class SynchronizedHashMapWithReadWriteLock {

    private static final Logger LOGGER = Logger.getLogger("SyncMap");
    private Map<String, String> syncHashMap = new HashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock writeLock = lock.writeLock();
    private Lock readLock = lock.readLock();

    public void put(String key, String value) {

        try {
            writeLock.lock();
            readLock.lock();
            syncHashMap.put(key, value);
        } finally {
            writeLock.unlock();
            readLock.unlock();
        }
    }

    public String remove(String key) {
        try {
            writeLock.lock();
            readLock.lock();
            return syncHashMap.remove(key);
        } finally {
            writeLock.unlock();
            readLock.unlock();
        }
    }

    public String get(String key) {
        try {
            readLock.lock();
            return syncHashMap.get(key);
        } finally {
            readLock.unlock();
        }
    }

    public boolean containsKey(String key) {
        try {
            readLock.lock();
            return syncHashMap.containsKey(key);
        } finally {
            readLock.unlock();
        }
    }
}