package com.bunis.threads.state;

import org.apache.log4j.Logger;

public class WaitingStateDemo implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(WaitingStateDemo.class);

    private static Thread t1;

    public static void main(String[] args) {
        t1 = new Thread(new WaitingStateDemo());
        t1.start();
    }

    @Override
    public void run() {
        Thread t2 = new Thread(new DemoThreadWS());
        t2.start();

        try {
            t2.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.error("Thread interrupted", e);
        }
    }

    class DemoThreadWS implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                LOGGER.error("Thread interrupted", e);
            }
            LOGGER.info(WaitingStateDemo.t1.getState());
        }
    }
}