package com.bunis.threads.simple;

import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.Random;

public class SimpleRunnable implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(SimpleRunnable.class);
    private static final String HELLO_FROM_RUNNABLE_MEG = "Hello from runnable {0}";

    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {

            try {
                long millis = Math.abs(new Random().nextInt(2000));
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                LOGGER.error(e);
                break;
            }
            LOGGER.info(MessageFormat.format(HELLO_FROM_RUNNABLE_MEG, i));
        }
    }
}
