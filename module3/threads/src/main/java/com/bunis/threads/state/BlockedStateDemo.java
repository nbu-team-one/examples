package com.bunis.threads.state;

import org.apache.log4j.Logger;

public class BlockedStateDemo {

    private static final Logger LOGGER = Logger.getLogger(BlockedStateDemo.class);

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new SimpleBlockedRunnable());
        Thread t2 = new Thread(new SimpleBlockedRunnable());

        t1.start();
        t2.start();

        Thread.sleep(1000);

        LOGGER.info(t2.getState());
        System.exit(0);
    }
}