package com.bunis.threads.proxy;

import java.beans.Transient;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayDeque;
import java.util.Queue;

public class EntityServiceProxyFactory {

    EntityService first = new FirstEntityService();
    EntityService second = new SecondEntityService();

    private EntityService getConcrete(String name) {
//        switch (name)
        return null;
    }

    public EntityService getProxy(String beanName) {

        return (EntityService) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{EntityService.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String name = method.getName();
                if ("save".equals(name)) {
                    // LOG
                }

                return method.invoke(getConcrete(beanName), args);
            }
        });
    }
}
