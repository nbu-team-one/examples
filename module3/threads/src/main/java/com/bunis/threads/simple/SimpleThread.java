package com.bunis.threads.simple;

import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.Random;

public class SimpleThread extends Thread {

    private static final Logger LOGGER = Logger.getLogger(SimpleThread.class);
    private static final String HELLO_FROM_THREAD_MEG = "Hello from thread {0}";
    private static final String SLEEP_FOR_MSG = "sleep for {0}";
    private final int count;
    public long sleepTime = 0;

    public SimpleThread(String name) {
        super(name);
        this.count = 5;
    }

    public SimpleThread(String name, int count) {
        super(name);
        this.count = count;
    }

    @Override
    public void run() {

        for (int i = 0; i < this.count; i++) {
            try {
                long millis = sleepTime > 0 ? sleepTime : Math.abs(new Random().nextInt(2000));
                LOGGER.info(MessageFormat.format(SLEEP_FOR_MSG, millis));
                if (!isInterrupted()) {
                    sleep(millis);
                }
            } catch (InterruptedException e) {
                LOGGER.error(e);
                break;
            }
            LOGGER.info(MessageFormat.format(HELLO_FROM_THREAD_MEG, i));
        }
    }
}
