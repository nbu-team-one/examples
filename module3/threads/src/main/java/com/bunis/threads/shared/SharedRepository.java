package com.bunis.threads.shared;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class SharedRepository {

    private static final AtomicLong counter = new AtomicLong(0);

    public static List<String> getMessages() {

        synchronized (SharedRepository.class) {
            long l = counter.incrementAndGet();
            List<String> result = new ArrayList<>((int) l);
            for (int i = 0; i < l; i++) {
                result.add("S" + i);
            }
            return result;
        }
    }
}
