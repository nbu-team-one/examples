package com.bunis.threads.state;

import org.apache.log4j.Logger;

public class TerminatedStateDemo implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(TerminatedStateDemo.class);


    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new TerminatedStateDemo());
        t1.start();
        // The following sleep method will give enough time for
        // thread t1 to complete
        Thread.sleep(1000);
        LOGGER.info(t1.getState());
    }

    @Override
    public void run() {
        // No processing in this block
    }
}