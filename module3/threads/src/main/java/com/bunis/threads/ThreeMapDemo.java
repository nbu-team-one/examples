package com.bunis.threads;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ThreeMapDemo {

    public static void main(String[] args) {


        Map<Person, List<Person>> persons = new TreeMap<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return Integer.compare(o1.age, o2.age);
            }
        });

        Set<Person> list = new HashSet<>();
    }

    static class Person {

        private int age;

        public Person(int age) {
            this.age = age;
        }

        public int getAge() {
            return age;
        }
    }
}
