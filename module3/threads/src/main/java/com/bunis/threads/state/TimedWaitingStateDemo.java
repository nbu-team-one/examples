package com.bunis.threads.state;
import org.apache.log4j.Logger;

public class TimedWaitingStateDemo {
    private static final Logger LOGGER = Logger.getLogger(TimedWaitingStateDemo.class);
    public static void main(String[] args) throws InterruptedException {
        TimedThread obj1 = new TimedThread();
        Thread t1 = new Thread(obj1);
        t1.start();
         
        // The following sleep will give enough time for ThreadScheduler
        // to start processing of thread t1
        Thread.sleep(1000);
        LOGGER.info(t1.getState());
    }

    static class TimedThread implements Runnable {
        @Override
        public void run() {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                LOGGER.error("Thread interrupted", e);
            }
        }
    }
}