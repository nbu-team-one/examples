package com.bunis.threads.simple;

import org.apache.log4j.Logger;

import java.util.Random;
import java.util.concurrent.ExecutionException;

public class SimpleThreadDemo {
    private static final Logger LOGGER = Logger.getLogger(SimpleThreadDemo.class);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new SimpleThreadDemo().showDemo();
    }

    public void showDemo() throws ExecutionException, InterruptedException {


//        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
//
//         executorService.scheduleAtFixedRate(() -> {
//            // do some staff
//            LOGGER.info("message from future task");
//
//        }, 1, 2, TimeUnit.SECONDS);
//
//
//        executorService.awaitTermination(10, TimeUnit.SECONDS);
//        executorService.shutdown();

        LOGGER.info("Start from main");
        SimpleThread t1 = new SimpleThread("T1");
        t1.start();

        Runnable r1 = () -> {
            while (true) {

                long millis = Math.abs(new Random().nextInt(2000));
                LOGGER.info("RUNNABLE " + millis);
//                try {
//                    Thread.currentThread().sleep(millis);
//
//                } catch (InterruptedException e) {
//                    throw new IllegalThreadStateException(e.getMessage());
//                }
                if (millis < 300) {
                    LOGGER.info("RUNNABLE interrupt");
                    Thread.currentThread().stop();
                }
            }
        };
        Thread t2 = new Thread(r1, "T2");
        t2.setDaemon(true);
        t2.start();
        t2.sleep(1000, 10000);

        LOGGER.info("End from main");
    }

}
