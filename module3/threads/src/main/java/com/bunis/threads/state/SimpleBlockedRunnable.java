package com.bunis.threads.state;

public class SimpleBlockedRunnable implements Runnable {
    public static synchronized void commonResource() {
        while (true) {
            // Infinite loop to mimic heavy processing
            // 't1' won't leave this method
            // when 't2' try to enters this
        }
    }

    @Override
    public void run() {
        commonResource();
    }
}