package com.bunis.threads.shared;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Random;

public class SharedRepositoryDemo {

    private static final Logger LOGGER = Logger.getLogger(SharedRepositoryDemo.class);

    public static void main(String[] args) {


        Runnable r1 = () -> {

            for (int i = 0; i < 10; i++) {
                long millis = Math.abs(new Random().nextInt(2000));
                List<String> messages = SharedRepository.getMessages();
                for (String message : messages) {
                    LOGGER.info("From Runnable: " + message);
                }
                try {
                    Thread.currentThread().sleep(millis);
                } catch (InterruptedException e) {
                    throw new IllegalThreadStateException(e.getMessage());
                }
            }
        };

        Thread t1 = new Thread(r1, "T1");
        Thread t2 = new Thread(r1,"T2");

        LOGGER.info("Starting");
        t1.start();
        t2.start();

    }
}
