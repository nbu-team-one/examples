package com.bunis.threads.proxy;

import java.util.List;

public interface EntityService {

    void save(EntityObject object);

    List<EntityObject> readAll();
}
