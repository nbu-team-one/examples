package com.bunis.threads.simple;

import com.bunis.threads.shared.SharedRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RunWith(JUnit4.class)
public class SimpleThreadDemoTest {

//    @Test
    public void demo1() throws ExecutionException, InterruptedException {

        SimpleThreadDemo threadDemo = new SimpleThreadDemo();
        threadDemo.showDemo();
    }

//    @Test
    public void demo2() throws InterruptedException {

        SimpleThread t1 = new SimpleThread("t1");
        t1.start();
        Thread.currentThread().join();
    }

    public void demo3() {


    }

}
