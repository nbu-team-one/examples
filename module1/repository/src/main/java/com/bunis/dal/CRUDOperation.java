package com.bunis.dal;

public interface CRUDOperation<T> {

    default boolean save(T entity) {
        return false;
    }

    default T read(Long entityId) {
        return null;
    }

    default boolean update(T entity) {
        return false;
    }

    default boolean delete(T entity) {
        return false;
    }
}
