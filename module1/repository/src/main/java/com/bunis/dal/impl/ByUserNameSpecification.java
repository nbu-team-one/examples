package com.bunis.dal.impl;

import com.bunis.dal.Specification;
import com.bunis.bean.User;

public class ByUserNameSpecification implements Specification<User> {

    private String searchName;

    public ByUserNameSpecification(String searchName) {
        this.searchName = searchName;
    }

    public boolean match(User bean) {
        return this.searchName.equals(bean.getName());
    }
}
