package com.bunis.dal;

import java.util.List;

public interface Repository<T> extends CRUDOperation<T> {

    List<T> find(Specification<T> spec);
}
