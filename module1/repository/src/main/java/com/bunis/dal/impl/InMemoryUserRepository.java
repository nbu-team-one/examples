package com.bunis.dal.impl;

import com.bunis.dal.Repository;
import com.bunis.dal.Specification;
import com.bunis.bean.User;

import java.util.ArrayList;
import java.util.List;

public class InMemoryUserRepository implements Repository<User> {

    List<User> users = new ArrayList<User>();

    {
        User u1 = new User();
        u1.setAge(54);
        u1.setName("Jon");
        users.add(u1);

        User u2 = new User();
        u2.setAge(23);
        u2.setName("Ann");
        users.add(u2);

        User u3 = new User();
        u3.setAge(13);
        u3.setName("Juli");
        users.add(u3);
    }

    @Override
    public List<User> find(Specification<User> spec) {

        List<User> result = new ArrayList<>();

        for (User user : users) {
            if (spec.match(user)) {
                result.add(user);
            }
        }

        return result;
    }
}
