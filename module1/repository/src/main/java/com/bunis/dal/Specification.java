package com.bunis.dal;

@FunctionalInterface
public interface Specification<T> {

    boolean match(T bean);
}
