package com.bunis.dal;

import com.bunis.bean.User;

import java.util.List;

public interface UserDao extends CRUDOperation<User> {

    List<User> findNewUsers();

    boolean blockUserByEmail(String email);

    boolean blockUserByLogin(String login);

    User findByCriteria(String email);

    User findByLogin(String login);
}
