package com.bunis.dal;

import com.bunis.bean.User;
import com.bunis.dal.impl.ByUserNameSpecification;
import com.bunis.dal.impl.InMemoryUserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith(JUnit4.class)
public class InMemoryUserRepositoryTest {

    @Test
    public void shouldFindJon() {

        Repository<User> userRepository = new InMemoryUserRepository();
        Specification<User> userSpecification = bean -> bean.getName().equals("Jon");
        List<User> jonList = userRepository.find(userSpecification);

        Assert.assertEquals(1, jonList.size());
    }

    @Test
    public void shouldFindAnnAndSomeAdult() {

        final Repository<User> userRepository = new InMemoryUserRepository();

        final Specification<User> byName = new ByUserNameSpecification("Ann");

        final Specification<User> byAge = new Specification<User>() {
            public boolean match(User bean) {
                return bean.getAge() > 30;
            }
        };

        Specification<User> or = new Specification<User>() {
            public boolean match(User bean) {
                return byName.match(bean) || byAge.match(bean);
            }
        };
        List<User> list = userRepository.find(or);
        Assert.assertEquals(2, list.size());
    }
}
