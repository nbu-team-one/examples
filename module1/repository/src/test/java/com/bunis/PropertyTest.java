package com.bunis;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

@RunWith(JUnit4.class)
public class PropertyTest {

    @Test
    public void checkProperties() throws IOException {

        URL resource = getClass().getResource("/repo.properties");
        Properties props = new Properties();
        props.load(resource.openStream());
        String testList = props.getProperty("com.test.properties");
        Set<String> collect = Arrays.stream(testList.split(","))
                .map(s -> props.getProperty("com.test.properties." + s))
                .collect(Collectors.toSet());

        Assert.assertTrue(collect.contains("third"));
    }
}
