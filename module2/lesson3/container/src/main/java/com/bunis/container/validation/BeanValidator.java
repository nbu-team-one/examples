package com.bunis.container.validation;

public interface BeanValidator {

    ValidationResult validate(Object bean);
}
