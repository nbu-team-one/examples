package com.bunis.container;

public class AppConstants {

    public static final String EMAIL_PATTERN = "^[A-Z0-9._]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
}
