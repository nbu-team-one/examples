package com.bunis.module2.model;

import com.bunis.module2.composite.LeafEmployee;
import com.bunis.module2.composite.SmartEmployee;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class Director extends SmartEmployee {

    private static final Logger LOG = Logger.getLogger(Director.class);
    private final List<LeafEmployee> employees = new LinkedList<>();
    private final String name;

    public Director(String name, EmployeeJobFunction jobFunction) {
        super(jobFunction);
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {
        employees.add(employee);
    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return this.employees;
    }

    @Override
    public void printName() {
        // print current name
        LOG.info("I'm SimpleDirector " + name + " and here are my managers:");
        // call #printName() for all managers
        employees.forEach(LeafEmployee::printName);
    }
}
