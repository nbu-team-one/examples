package com.bunis.module2.model;

import com.bunis.module2.composite.LeafEmployee;
import com.bunis.module2.composite.SmartEmployee;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

public class Worker extends SmartEmployee {

    private static final Logger LOG = Logger.getLogger(Worker.class);

    private final String name;

    public Worker(String name, EmployeeJobFunction jobFunction) {
        super(jobFunction);
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {

    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return Collections.emptyList();
    }

    @Override
    public void printName() {

        LOG.info("I'm worker " + name);
    }
}
