package com.bunis.module2.chain;

public interface ParserChain<T> { 
    // <T> is generic typization, in that case our ParserChain can produce any Object

    /**
     * In implementation of this method line will be converted into employee
     *
     * @param line
     * @return
     */
    T parseLine(String line);

    /**
     * Links current parser with next and makes chain
     * @param next
     * @return
     */
    ParserChain<T> linkWith(ParserChain<T> next);
}
