package com.bunis.module2.composite;

import java.util.List;

/**
 * Simple interpretation of composite design
 */
public interface CompositeEmployee extends LeafEmployee {

    /**
     * Add someone to employees
     *
     * @param employee
     */
    void addEmployee(LeafEmployee employee);

    /**
     * Get all possible employees
     *
     * @return list of employees
     */
    List<LeafEmployee> getEmployees();
}
