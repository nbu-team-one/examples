package com.bunis.module2.model;

import com.bunis.module2.composite.LeafEmployee;
import com.bunis.module2.composite.SmartEmployee;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class Manger extends SmartEmployee {

    private static final Logger LOG = Logger.getLogger(Manger.class);
    private final String name;
    private final List<LeafEmployee> employees = new LinkedList<>();

    public Manger(String name, EmployeeJobFunction jobFunction) {
        super(jobFunction);
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {

        employees.add(employee);
    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return employees;
    }

    @Override
    public void printName() {
        LOG.info("I'm a manager " + name + " and here are my people:");
        this.employees.forEach(LeafEmployee::printName);
    }
}
