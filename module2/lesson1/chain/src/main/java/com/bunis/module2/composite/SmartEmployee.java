package com.bunis.module2.composite;

public abstract class SmartEmployee implements CompositeEmployee {

    /**
     * Current job function
     */
    private final EmployeeJobFunction jobFunction;
    /**
     * Link to the parent employee (worker->lead, manager->director)
     */
    private SmartEmployee parent;
    /**
     * Link to last added child
     */
    private SmartEmployee lastChild;

    public SmartEmployee(EmployeeJobFunction jobFunction) {
        this.jobFunction = jobFunction;
    }

    /**
     * Receives employee and find place where to add him
     * @param employee
     */
    public void smartAddEmployee(SmartEmployee employee) {
        // understands difference in job function
        int compare = Integer.compare(this.jobFunction.level, employee.jobFunction.level);
        switch (compare) {
            case 0:
                // same job function
                // check if parent exists
                if (parent != null) {
                    // add colleague to the parent
                    parent.addEmployee(employee);
                    // and makes required links
                    employee.parent = parent;
                    parent.lastChild = employee;
                }
                break;
            case 1:
                // check if new employee cannot be added directly to the parent
                // for example now we are Worker and receive Manager
                // we are going to escalate adding new employee to the parent
                if (parent != null) {
                    // escalate
                    parent.smartAddEmployee(employee);
                }
                break;
            case -1:
                // first check if don't have any children: if yes then just and child employee to the current
                // or we already have some children and new employee has the same job function as our children
                if (lastChild == null || lastChild.jobFunction.level == employee.jobFunction.level) {
                    // just add employee
                    addEmployee(employee);
                    // and make links
                    employee.parent = this;
                    lastChild = employee;
                } else {
                    // if new employee has job function lower then in our children
                    // then add new employee to our last child
                    lastChild.smartAddEmployee(employee);
                }
                break;
            default:
                // Impossible line, but shit happens
                throw new IllegalStateException("Should never occur");
        }
    }

    /**
     * Here we will find our root employee
     * @return employee
     */
    public SmartEmployee findRoot() {
        // first check if we already don't have parent
        if (this.parent == null) {
            return this;
        } else {
            // we will find him
            SmartEmployee director = this.parent;
            while (director.parent != null) {
                director = director.parent;
            }
            return director;
        }
    }

    /**
     * Enum of all possible jobs in our application
     * In future can be replaced with another model for scalability maintenance
     */
    public enum EmployeeJobFunction {
        DIRECTOR("Director", 1),
        MANAGER("Manager", 2),
        LEAD("Lead", 3),
        WORKER("Worker", 4);

        private final int level;
        private final String title;

        EmployeeJobFunction(String title, int level) {
            this.title = title;
            this.level = level;
        }

        public int getLevel() {
            return this.level;
        }

        public String getTitle() {
            return title;
        }
    }
}
