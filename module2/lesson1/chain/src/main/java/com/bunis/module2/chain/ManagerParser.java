package com.bunis.module2.chain;

import com.bunis.module2.composite.SmartEmployee;
import com.bunis.module2.model.Manger;
import org.apache.commons.lang3.StringUtils;

import static com.bunis.module2.composite.SmartEmployee.EmployeeJobFunction.MANAGER;

public class ManagerParser extends EmployeeParser {
    @Override
    public SmartEmployee parseLine(String line) {
        if (StringUtils.startsWith(line, MANAGER.getTitle())) {
            String name = line.split("-")[1];
            Manger manger = new Manger(name, MANAGER);
            return manger;
        }
        return nextParse(line);
    }
}
