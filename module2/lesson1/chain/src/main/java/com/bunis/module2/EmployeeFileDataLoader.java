package com.bunis.module2;

import com.bunis.module2.chain.ParserChain;
import com.bunis.module2.composite.SmartEmployee;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class EmployeeFileDataLoader {

    private final Path filePath;

    public EmployeeFileDataLoader(Path filePath) {
        this.filePath = filePath;
    }

    /**
     * Here we load the file and convert lines to employees using parser chain
     *
     * @param parserChain - newly created parser chain
     * @return employees
     * @throws IOException
     */
    public SmartEmployee loadEmployees(ParserChain<SmartEmployee> parserChain) throws IOException {

        SmartEmployee root = null;
        // read all lines
        List<String> allLines = Files.readAllLines(filePath);
        // go through the lines and collect employees using smart adding
        for (String line : allLines) {
            // parse line using chain
            SmartEmployee employee = parserChain.parseLine(line);
            if (employee == null) {
                // if we didn't parse employee then go to the next line
                continue;
            }
            // check if we don't have any parsed employee
            if (root == null) {
                root = employee;
                continue;
            }
            // otherwise add to previously parsed employee new one
            root.smartAddEmployee(employee);
            // change link
            root = employee;
        }

        // if we found someone let's find and return director
        return root != null ? root.findRoot() : null;
    }
}
