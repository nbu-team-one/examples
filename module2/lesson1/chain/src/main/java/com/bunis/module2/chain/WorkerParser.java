package com.bunis.module2.chain;

import com.bunis.module2.composite.SmartEmployee;
import com.bunis.module2.model.Worker;
import org.apache.commons.lang3.StringUtils;

import static com.bunis.module2.composite.SmartEmployee.EmployeeJobFunction.WORKER;

public class WorkerParser extends EmployeeParser {
    @Override
    public SmartEmployee parseLine(String line) {
        if (StringUtils.startsWith(line, WORKER.getTitle())) {
            String name = line.split("-")[1];
            Worker worker = new Worker(name, WORKER);
            return worker;
        }
        return nextParse(line);
    }
}
