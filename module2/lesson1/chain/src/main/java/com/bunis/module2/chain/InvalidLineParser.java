package com.bunis.module2.chain;

import com.bunis.module2.composite.SmartEmployee;
import org.apache.log4j.Logger;

public class InvalidLineParser extends EmployeeParser {

    private static final Logger LOG = Logger.getLogger(InvalidLineParser.class);

    @Override
    public SmartEmployee parseLine(String line) {
        LOG.warn("Invalid line\n" + line);
        return nextParse(line);
    }
}
