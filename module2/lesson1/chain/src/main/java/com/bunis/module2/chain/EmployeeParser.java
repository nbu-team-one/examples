package com.bunis.module2.chain;

import com.bunis.module2.composite.SmartEmployee;

public abstract class EmployeeParser implements ParserChain<SmartEmployee> {
    // take a look on the interface implementation - we generify ParserChain
    /**
     * Next parser
     */
    private EmployeeParser next;

    @Override
    public ParserChain<SmartEmployee> linkWith(ParserChain<SmartEmployee> next) {
        ((EmployeeParser) next).next = this;
        return next;
    }

    /**
     * If current doesn't parse line next parser will be invoked
     *
     * @param line
     * @return
     */
    protected SmartEmployee nextParse(String line) {

        // check if we have next parser
        if (next != null) {
            return next.parseLine(line);
        } else {
            return null;
        }
    }
}
