package com.bunis.module2.chain;

import com.bunis.module2.composite.SmartEmployee;
import com.bunis.module2.model.Lead;
import org.apache.commons.lang3.StringUtils;

import static com.bunis.module2.composite.SmartEmployee.EmployeeJobFunction.LEAD;

public class LeadParser extends EmployeeParser {
    @Override
    public SmartEmployee parseLine(String line) {
        if (StringUtils.startsWith(line, LEAD.getTitle())) {
            String name = line.split("-")[1];
            Lead lead = new Lead(name, LEAD);
            return lead;
        }
        return nextParse(line);
    }
}
