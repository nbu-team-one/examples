package com.bunis.module2.chain;

import com.bunis.module2.composite.SmartEmployee;
import com.bunis.module2.model.Director;
import org.apache.commons.lang3.StringUtils;

import static com.bunis.module2.composite.SmartEmployee.EmployeeJobFunction.DIRECTOR;

public class DirectorParser extends EmployeeParser {
    @Override
    public SmartEmployee parseLine(String line) {
        // recieves new line and check if we can parse it
        if (StringUtils.startsWith(line, DIRECTOR.getTitle())) {
            String name = line.split("-")[1];
            // creates and returns new Director
            Director director = new Director(name, DIRECTOR);
            return director;
        }
        // else try to parse line in next parser
        return nextParse(line);
    }
}
