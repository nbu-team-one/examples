package com.bunis.module2.model.smart;

import com.bunis.module2.composite.LeafEmployee;
import com.bunis.module2.composite.SmartEmployee;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

public class SmartWorker extends SmartEmployee {

    private static final Logger LOG = Logger.getLogger(SmartWorker.class);

    private final String name;

    public SmartWorker(String name, EmployeeJobFunction jobFunction) {
        super(jobFunction);
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {

    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return Collections.emptyList();
    }

    @Override
    public void printName() {

        LOG.info("I'm worker " + name);
    }
}
