package com.bunis.module2.model.simple;

import com.bunis.module2.composite.LeafEmployee;
import org.apache.log4j.Logger;

public class Worker implements LeafEmployee {

    private static final Logger LOG = Logger.getLogger(Worker.class);
    private final String name;

    public Worker(String name) {
        this.name = name;
    }

    @Override
    public void printName() {
        LOG.info("I'm worker " + name);
    }
}
