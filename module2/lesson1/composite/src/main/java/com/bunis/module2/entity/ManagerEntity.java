package com.bunis.module2.entity;

public class ManagerEntity {

    private final Long id;
    private final String name;
    private final Long directorId;

    public ManagerEntity(Long id, String name, Long directorId) {
        this.id = id;
        this.name = name;
        this.directorId = directorId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getDirectorId() {
        return directorId;
    }
}
