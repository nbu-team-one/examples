package com.bunis.module2;

import java.util.List;

public interface Repository<T> {

    Long save(T e);

    T load(Long id);

    List<T> find();
}



