package com.bunis.module2.model.smart;

import com.bunis.module2.composite.LeafEmployee;
import com.bunis.module2.composite.SmartEmployee;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class SmartDirector extends SmartEmployee {

    private static final Logger LOG = Logger.getLogger(SmartDirector.class);
    private final List<LeafEmployee> employees = new LinkedList<>();
    private final String name;

    public SmartDirector(String name, EmployeeJobFunction jobFunction) {
        super(jobFunction);
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {
        employees.add(employee);
    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return this.employees;
    }

    @Override
    public void printName() {
        // print current name
        LOG.info("I'm Director " + name + " and here are my managers:");
        // call #printName() for all managers
        employees.forEach(LeafEmployee::printName);
    }
}
