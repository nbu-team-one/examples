package com.bunis.module2.entity;

public class DirectorEntity {

    private final Long id;
    private final String name;

    public DirectorEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
