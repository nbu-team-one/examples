package com.bunis.module2.model.simple;

import com.bunis.module2.composite.CompositeEmployee;
import com.bunis.module2.composite.LeafEmployee;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class Director implements CompositeEmployee {

    private static final Logger LOG = Logger.getLogger(Director.class);
    private final String name;
    private final List<LeafEmployee> managers = new LinkedList<>(); // list of child leafs

    public Director(String name) {
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {
        this.managers.add(employee);
    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return this.managers;
    }

    @Override
    public void printName() {
        // print current name
        LOG.info("I'm Director " + name + " and here are my managers:");
        // call #printName() for all managers
        managers.forEach(LeafEmployee::printName);
    }
}
