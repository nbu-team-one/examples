package com.bunis.module2.composite;

public interface LeafEmployee {

    /**
     * Just for the demo print all possible names
     */
    void printName();
}
