package com.bunis.module2.model.simple;

import com.bunis.module2.composite.CompositeEmployee;
import com.bunis.module2.composite.LeafEmployee;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class Manager implements CompositeEmployee {

    private static final Logger LOG = Logger.getLogger(Manager.class);
    private final String name;
    private final List<LeafEmployee> workers = new LinkedList<>(); // list of child leafs

    public Manager(String name) {
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {
        this.workers.add(employee);
    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return this.workers;
    }

    @Override
    public void printName() {
        LOG.info("I'm a manager " + name + " and here are my workers:");
        this.workers.forEach(LeafEmployee::printName);
    }
}
