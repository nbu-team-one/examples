package com.bunis.module2.model.smart;

import com.bunis.module2.composite.LeafEmployee;
import com.bunis.module2.composite.SmartEmployee;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class SmartLead extends SmartEmployee {

    private static final Logger LOG = Logger.getLogger(SmartLead.class);

    private final List<LeafEmployee> employees = new LinkedList<>();
    private final String name;

    public SmartLead(String name, EmployeeJobFunction jobFunction) {
        super(jobFunction);
        this.name = name;
    }

    @Override
    public void addEmployee(LeafEmployee employee) {

        employees.add(employee);
    }

    @Override
    public List<LeafEmployee> getEmployees() {
        return employees;
    }

    @Override
    public void printName() {

        LOG.info("I'm a lead " + name + " and here are my people:");
        this.employees.forEach(LeafEmployee::printName);
    }
}
