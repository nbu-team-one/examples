package com.bunis.module2;

import com.bunis.module2.composite.SmartEmployee;
import com.bunis.module2.model.smart.SmartDirector;
import com.bunis.module2.model.smart.SmartLead;
import com.bunis.module2.model.smart.SmartManger;
import com.bunis.module2.model.smart.SmartWorker;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static com.bunis.module2.composite.SmartEmployee.EmployeeJobFunction.*;

public class EmployeeFileDataLoader {

    private static final Logger LOG = Logger.getLogger(EmployeeFileDataLoader.class);
    private final Path filePath;

    public EmployeeFileDataLoader(Path filePath) {
        this.filePath = filePath;
    }

    /**
     * Here we load the file and convert lines to employees
     *
     * @return employees
     * @throws IOException
     */
    public SmartEmployee loadEmployees() throws IOException {

        List<String> allLines = Files.readAllLines(filePath);

        SmartEmployee root = null;
        // go through the lines and collect employees using smart adding
        for (String line : allLines) {
            // parse line
            SmartEmployee employee = parseLine(line);
            if (employee == null) {
                // if we didn't parse employee then go to the next line
                continue;
            }
            // check if we don't have any parsed employee
            if (root == null) {
                root = employee;
                continue;
            }
            // otherwise add to previously parsed employee new one
            root.smartAddEmployee(employee);
            // change link
            root = employee;
        }

        // if we found someone let's find and return director
        return root != null ? root.findRoot() : null;
    }

    /**
     * Here we going to convert line into the specific employee
     * We check with what string each line starts and according that create specific employee
     *
     * @param line
     * @return employee or null if line cannot be parsed
     */
    private SmartEmployee parseLine(String line) {
        if (StringUtils.startsWith(line, DIRECTOR.getTitle())) {
            String name = line.split("-")[1];
            SmartDirector smartDirector = new SmartDirector(name, DIRECTOR);
            return smartDirector;
        } else if (StringUtils.startsWith(line, MANAGER.getTitle())) {
            String name = line.split("-")[1];
            SmartManger manger = new SmartManger(name, MANAGER);
            return manger;
        } else if (StringUtils.startsWith(line, LEAD.getTitle())) {
            String name = line.split("-")[1];
            SmartLead smartLead = new SmartLead(name, LEAD);
            return smartLead;
        } else if (StringUtils.startsWith(line, WORKER.getTitle())) {
            String name = line.split("-")[1];
            SmartWorker smartWorker = new SmartWorker(name, WORKER);
            return smartWorker;
        } else {
            LOG.warn("Cannot parse line \n" + line);
            return null;
        }
    }
}
