package com.bunis.module2;

import com.bunis.module2.composite.CompositeEmployee;
import com.bunis.module2.composite.LeafEmployee;
import com.bunis.module2.model.simple.Director;
import com.bunis.module2.model.simple.Manager;
import com.bunis.module2.model.simple.Worker;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;

@RunWith(JUnit4.class)
public class CompositeEmployeeTest {


    @Test
    public void printEmployeesNames() throws IOException {

        LeafEmployee employee = getCompositeDirector();
        employee.printName();
        Assert.assertFalse(((CompositeEmployee) employee).getEmployees().isEmpty());
    }

    private LeafEmployee getCompositeDirector() {
        // our root employee
        CompositeEmployee director = new Director("Averil");
        // First-level child
        CompositeEmployee manager = new Manager("Wenda");
        director.addEmployee(manager);
        // Third-level children
        manager.addEmployee(new Worker("Artur"));
        manager.addEmployee(new Worker("Douglass"));
        manager.addEmployee(new Worker("Carrol"));
        return director;
    }
}
