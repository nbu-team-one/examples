package com.bunis.module2;

import com.bunis.module2.composite.SmartEmployee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RunWith(JUnit4.class)
public class SmartEmployeeTest {

    private Path resourcePath;

    @Before
    public void prepareData() throws URISyntaxException {

        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("smart-employee.list").toURI());
    }

    @Test
    public void printEmployeesNames() throws IOException {

        SmartEmployee employee = new EmployeeFileDataLoader(resourcePath).loadEmployees();
        employee.printName();
        Assert.assertFalse(employee.getEmployees().isEmpty());
    }
}
