package com.bunis.modulel2.lambda;

import java.util.function.Function;

@FunctionalInterface
public interface IdToPersonMapper extends Function<Long, Person> {
}
