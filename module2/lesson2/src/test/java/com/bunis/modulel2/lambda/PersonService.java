package com.bunis.modulel2.lambda;

public interface PersonService {

    Person loadById(Long id);
}
