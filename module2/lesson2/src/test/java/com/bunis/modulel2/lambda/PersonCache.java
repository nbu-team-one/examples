package com.bunis.modulel2.lambda;

import java.util.HashMap;
import java.util.Map;

public class PersonCache {

    Map<Long, Person> personCache = new HashMap<>();

    IdToPersonMapper mapFunction;


    public PersonCache(IdToPersonMapper mapFunction) {
        this.mapFunction = mapFunction;
    }

    public Person getPerson(Long id) {

        return personCache.computeIfAbsent(id, mapFunction);
    }
}
