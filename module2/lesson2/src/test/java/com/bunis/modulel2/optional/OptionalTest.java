package com.bunis.modulel2.optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@RunWith(JUnit4.class)
public class OptionalTest {

    @Test
    public void checkOptionalString() {

        Date now = new Date();
        String formatted = MessageFormat.format("Now is {0} (unformatted)", now);
        System.out.println(formatted);
    }


    public Optional<Employee> getEmployee() {

        Employee e = new Employee("Jhon");

        List<AddressLine> lines = IntStream.range(1, 5).mapToObj(i -> "Street" + i)
                .map(AddressLine::new)
                .collect(Collectors.toList());

        e.addresses = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            e.addresses.add(new Address(lines));
        }

        return Optional.ofNullable(e);
    }


    @Test
    public void testAddress() {

        List<AddressLine> collect = getEmployee()
                .flatMap(e -> Optional.ofNullable(e.addresses))
                .map(addresses -> addresses.stream())
                .orElse(Stream.empty())
                .flatMap(address -> address.lines.stream())
                .collect(Collectors.toList());
        collect.forEach(line -> System.out.println(line.street));
    }
}
