package com.bunis.modulel2.lambda;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class FunctionTest {

    Supplier<PersonService> lazyService = () -> null;

    PersonService personService = (id) -> {
        Person p = new Person();
        p.id = id;
        p.name = "Name" + id;
        return p;
    };

    @Test
    public void useCache() {

        Person expected = new Person();
        expected.id = 1L;
        expected.name = "Name1";

        PersonCache cache = new PersonCache(personService::loadById);
        Person actual = cache.getPerson(1L);
        Assert.assertEquals(expected, actual);
    }

    private void foo(String s1) {

        // find entity by s1
        Supplier<Person> lazyDefaultLoad = () -> personService.loadById(1L);
        Person defaultPerson = personService.loadById(1L);

        // find here
        // find some where else
        Person person = lazyDefaultLoad.get();
    }
}
