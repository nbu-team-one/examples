package com.bunis.modulel2.lambda;

public class ComplexModel {

    private String name;
    private ModelType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelType getType() {
        return type;
    }

    public void setType(ModelType type) {
        this.type = type;
    }

    public enum ModelType {

        FIRST(1),
        SECOND(2),
        THIRD(3);

        private final int type;

        ModelType(int type) {
            this.type = type;
        }

        public static ModelType ofInt(int type) {
            for (ModelType value : values()) {
                if (value.type == type) {
                    return value;
                }
            }
            return null;
        }

        public int getType() {
            return type;
        }
    }
}
