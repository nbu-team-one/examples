package com.bunis.modulel2.optional;

import java.util.Optional;

public interface SearchService {

    Optional<Employee> findByName(String name);
}
