package com.bunis.modulel2.optional;

import java.util.List;

public class Employee {

    private final String name;

    List<Address> addresses;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
