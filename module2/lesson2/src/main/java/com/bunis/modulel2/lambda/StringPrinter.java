package com.bunis.modulel2.lambda;

public interface StringPrinter {

    void printString(String line);
}
