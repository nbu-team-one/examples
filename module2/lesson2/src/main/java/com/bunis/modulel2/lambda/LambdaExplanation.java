package com.bunis.modulel2.lambda;

public class LambdaExplanation {

    /**
     * Anonymous class creation
     */
    public StringCreator getStringCreator() {

        StringCreator stringCreator = new StringCreator() {
            @Override
            public String createString() {
                return "Hello from anonymous class";
            }
        };
        return stringCreator;
    }

    /**
     * Lambda creation
     */
    public StringCreator getLambdaStringCreator() {

        // lambda body surrounded with braces {}
        StringCreator stringCreator = () -> {
            return "Hello from lambda";
        };
        // StringCreator stringCreator = () -> "Hello from lambda";
        return stringCreator;
    }

    public StringConcatinator getStringConcatinator() {

        StringConcatinator stringConcatinator = new StringConcatinator() {
            @Override
            public String concatWith(String another) {
                return "I'm first and second is " + another;
            }
        };
        return stringConcatinator;
    }

    public StringConcatinator getLambdaStringConcatinator() {

        // braces are not required if interface method contains only one parameter
        // StringConcatinator stringConcatinator = another -> "Lambda first and second is " + another;
        StringConcatinator stringConcatinator = (another) -> "Lambda first and second is " + another;
        return stringConcatinator;
    }

    public StringCombiner getStringCombiner() {

        StringCombiner stringCombiner = new StringCombiner() {
            @Override
            public String combine(String first, String second) {
                return first + " " + second;
            }
        };
        return stringCombiner;
    }

    public StringCombiner getLambdaStringCombiner() {

        // when interface method has two or more parameters braces are required (first, second)
        StringCombiner stringCombiner = (first, second) -> first + " from lambda with love " + second;
        return stringCombiner;
    }

    public StringPrinter getStringPrinter() {
        StringPrinter stringPrinter = new StringPrinter() {
            @Override
            public void printString(String line) {
                // print line
            }
        };

        return stringPrinter;
    }

    public StringPrinter getLambdaStringPrinter() {
        // take a look on the empty lambda body { }
        StringPrinter stringPrinter = line -> { };
        return stringPrinter;
    }

    private void printString(String line) {
        // print line
    }

    public StringPrinter getLambdaStringPrinterWithMethodRefernce() {
        StringPrinter stringPrinter = this::printString;
        return stringPrinter;
    }

    public ComplexModel buildComplexModel(String name, Integer type) {

        ComplexObjectBuilder builder = new ComplexObjectBuilder() {
            @Override
            public ComplexModel buildModel(String name, Integer type) {
                ComplexModel complexModel = new ComplexModel();
                complexModel.setName(name);
                ComplexModel.ModelType modelType = getModelType(type);
                complexModel.setType(modelType);
                return complexModel;
            }
        };

        return builder.buildModel(name, type);
    }

    public ComplexModel buildComplexModelWithLambda(String name, Integer type) {

        ComplexObjectBuilder builder = (nameInLambda, typeInLambda) -> {
            ComplexModel complexModel = new ComplexModel();
            complexModel.setName(nameInLambda);
            ComplexModel.ModelType modelType = getModelType(typeInLambda);
            complexModel.setType(modelType);
            return complexModel;
        };

        return builder.buildModel(name, type);
    }

    private ComplexModel.ModelType getModelType(Integer type1) {
        return ComplexModel.ModelType.ofInt(type1);
    }
}
