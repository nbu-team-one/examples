package com.bunis.modulel2.optional;

enum AddressType {

    MAIN,
    SECONDARY
}

public class AddressLine {

    String street;
    AddressType type;

    public AddressLine(String street) {
        this.street = street;
    }
}
