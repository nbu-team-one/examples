package com.bunis.modulel2.lambda;

@FunctionalInterface
public interface ComplexObjectBuilder {

    ComplexModel buildModel(String name, Integer type);
}
