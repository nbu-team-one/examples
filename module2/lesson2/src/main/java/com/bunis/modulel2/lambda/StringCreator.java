package com.bunis.modulel2.lambda;

    public interface StringCreator {

        String createString();
    }
