package com.bunis.modulel2.lambda;

public interface StringConcatinator {

    String concatWith(String another);
}
