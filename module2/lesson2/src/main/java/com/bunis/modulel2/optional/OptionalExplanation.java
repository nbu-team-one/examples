package com.bunis.modulel2.optional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class OptionalExplanation {

    private final static SearchService IN_MEMORY_SERVICE = new SearchService() {

        private List<Employee> employees = new LinkedList<>();

        {
            employees.add(new Employee("Jon"));
            employees.add(new Employee("Ann"));
        }

        @Override
        public Optional<Employee> findByName(String name) {
            return employees.stream().filter(e -> e.getName().equals(name)).findFirst();
        }
    };

    public void tryToFindAndWork(String name) {

        Optional<Employee> optionalEmployee = IN_MEMORY_SERVICE.findByName(name);
        optionalEmployee.ifPresent(this::doSomeWork);
    }


    private void doSomeWork(Employee employee) {

        // do work
    }
}